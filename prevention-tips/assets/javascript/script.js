$(document).ready(function(){
  $("#share").jsSocials({
      showLabel: false,
      showCount: false,
      shares: ["twitter", "facebook", "linkedin", "whatsapp"]
  });

  $('.launch-modal').each(function(){
    $(this).on('click', function(e){
        e.preventDefault();
        $( '#' + $(this).data('modal-id') ).modal();
    });
  })
});