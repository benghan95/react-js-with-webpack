import path from 'path';
import glob from 'glob-all';
import PurifyCSSPlugin from 'purifycss-webpack';
import webpack from 'webpack';
import bootstrapEntryPoints from './webpack.bootstrap.config.babel';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import HtmlWebpackPugPlugin from 'html-webpack-pug-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

let isProd = process.env.NODE_ENV === 'production';
let cssDev = [
  'style-loader', 
  'css-loader', 
  'postcss-loader', 
  'autoprefixer-loader',
  'sass-loader', 
];
let cssProd = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  use     : [
    {
      loader : 'css-loader',
    },
    {
      loader : 'postcss-loader',
    },
    {
      loader: 'autoprefixer-loader',
    },
    {
      loader : 'sass-loader',
    },
  ],
});

let cssConfig       = isProd ? cssProd : cssDev;
let bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;

module.exports = {
  entry : {
    bootstrap: bootstrapConfig,
    index: './src/index.js',
  },
  output: {
    path    : path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress   : true,
    hot        : true,
    open       : false
  },
  // devtool: 'source-map',
  module : {
    rules: [
      {
        test   : /\.(js|jsx|es6)$/,
        exclude: path.resolve(__dirname, "node_modules"),
        loader : 'babel-loader',
        query  : {
          presets: ['es2015', 'stage-0', 'react']
        }
      },
      {
        test: /\.pug$/,
        use : [
          'raw-loader',
          'pug-html-loader'
        ]
      },
      {
        test: /\.css$/,
        // exclude: path.resolve(__dirname, "node_modules"),
        use: cssConfig,
      },
      {
        test   : /\.scss$/,
        exclude: path.resolve(__dirname, "node_modules"),
        use    : cssConfig,
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        use : [
          'file-loader?name=images/[name].[ext]',
          'image-webpack-loader'
        ]
      },
      {
        test   : /\.svg$/,
        include: path.resolve(__dirname, "src/assets/images"),
        use    : [
          'file-loader?name=images/[name].[ext]',
        ]
      },
      {
        test   : /\.(mp3|wav)$/,
        include: path.resolve(__dirname, "src/assets/audio"),
        use    : [
          'file-loader?name=audio/[name].[ext]',
        ]
      },
      {
        test   : /\.mp4$/,
        include: path.resolve(__dirname, "src/assets/images"),
        use    : [
          'url-loader?name=videos/[name].[ext]&limit=10000&mimetype=video/mp4',
        ]
      },
      {
        test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/,
        use: 'imports-loader?jQuery=jquery'
      },
      {
        test   : /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        exclude: path.resolve(__dirname, "src/assets/images"),
        use    : [{
          loader : 'file-loader',
          options: {
            name      : '[name].[ext]',
            outputPath: 'fonts/',         // where the fonts will go
          }
        }]
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.es6'],
  },
  plugins      : [ 
    new HtmlWebpackPlugin({
      title         : 'Digi Predator',
      hash          : true,
      template      : './src/index.pug',
      chunksSortMode: (function orderEntryLast(a, b) {
        if (a.entry !== b.entry) {
          return b.entry ? 1: -1;
        } else {
          return b.id - a.id;
        }
      })
    }),
    // new HtmlWebpackPlugin({
    //   title   : 'Digi Tips',
    //   hash    : true,
    //   filename: 'tips.html',
    //   chunks  : ['tips'],
    //   template: './src/tips.pug'
    // }),
    new ExtractTextPlugin({
      filename:  (getPath) => {
        return getPath('css/[name].css').replace('css/js', 'css');
      },
      disable  : !isProd,
      allChunks: true,
    }),
    // new PurifyCSSPlugin({
    //   // Give paths to parse for rules. These should be absolute!
    //   paths: glob.sync([
    //     path.join(__dirname, 'src/*.pug')
    //   ]),
    // }),
    new HtmlWebpackPugPlugin(),
    new webpack.ProvidePlugin({
      $              : "jquery",
      jQuery         : "jquery",
      "window.jQuery": "jquery",
      Tether         : "tether",
      "window.Tether": "tether",
      Alert          : "exports-loader?Alert!bootstrap/js/dist/alert",
      Button         : "exports-loader?Button!bootstrap/js/dist/button",
      Carousel       : "exports-loader?Carousel!bootstrap/js/dist/carousel",
      Collapse       : "exports-loader?Collapse!bootstrap/js/dist/collapse",
      Dropdown       : "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
      Modal          : "exports-loader?Modal!bootstrap/js/dist/modal",
      Popover        : "exports-loader?Popover!bootstrap/js/dist/popover",
      Scrollspy      : "exports-loader?Scrollspy!bootstrap/js/dist/scrollspy",
      Tab            : "exports-loader?Tab!bootstrap/js/dist/tab",
      Tooltip        : "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
      Util           : "exports-loader?Util!bootstrap/js/dist/util",
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ]
}