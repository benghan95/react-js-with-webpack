import React, { Component } from 'react';
import Draggable from 'react-draggable';
import ProgressBar from 'progressbar.js';
import FontAwesome from 'react-fontawesome';

class ButtonDrag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posX: 0,
      posY: 0,
      progress: '',
    };
  }

  _handleDrag = (e, data) => {
    clearInterval(this.timer);
    this.setState({
      posX: data.x,
    });
    this._calcProgress();
  }

  _handleStop = (e, data) => {
    this.timer = setInterval(() => this._slideBack(), 50);
  }

  _slideBack = () => {
    if (this.state.posX > 9){
      this.setState({
        posX: this.state.posX - 10
      });
      this._calcProgress();
    }
    else if (this.state.posX > 0){
      this.setState({
        posX: 0
      });
      this._calcProgress();
    }
    else{
      clearInterval(this.timer);
    }
  }

  _calcProgress = () => {
    this.state.progress.set(this.state.posX / 100);
  }

  componentDidMount() {
    let progress = new ProgressBar.Circle('.drag-progress', {
      duration: 1000,
      fill: 'rgba(0, 0, 0, 0.3)',
      strokeWidth: 5,
      easing: 'easeInOut',
      color: '#fc0e29',
      trailColor: '#6b1a27',
      trailWidth: 1,
      svgStyle: {
        strokeDashoffset: 3,
        strokeDasharray: 5,
      },
    });

    this.setState({
      progress: progress
    });
  }

  render() {
    return (
      <div className="btn-drag">
        <div className="arrow">
          <Draggable
            axis="x"
            bounds={{ right: 100, left: 0 }}
            handle=".drag-progress"
            defaultPosition={{x: 0, y: 0}}
            position={{ x: this.state.posX, y: this.state.posY }}
            onStart={this._handleStart}
            onDrag={this._handleDrag}
            onStop={this._handleStop}>
            <div className="arrow-icon">
              <FontAwesome name='angle-right' />
            </div>
          </Draggable>
          <div className="arrow-line"></div>
        </div>
        <div className="drag-progress"></div>
      </div>
    );
  }
}

export default ButtonDrag;