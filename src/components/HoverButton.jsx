import React, { Component } from 'react';
import anime from 'animejs';

class HoverButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      circle     : null,
      position   : {},
      onHover    : false,
      onClick    : false,
      onComplete: false,
      onTempClick: false,
      timing     : 0,
      posX: 0,
      posY: 0,
    }
  }

  componentDidMount() {
    // this.setState({
    //   circle: this.props.circle
    // })
  }

  _onMouseMove = (e) => {
    // e.persist();
    // let diffX = e.pageX - e.target.offsetLeft;
    // let diffY = e.pageY - e.target.offsetTop;
    // console.log(e.target.offsetLeft);
    // console.log(e.target);
    // console.log(e);
    // console.log(e.pageX + " - " + e.target.offsetLeft + " = " + diffX);
    // console.log(e.pageY + " - " + e.target.offsetTop + " = " + diffY);
    // this.setState({
    //   position: {
    //     left: diffX + 'px',
    //     top : diffY + 'px',
    //   }
    // })
  }

  _onClick = (e) => {
    $('#dark-room').css("transform-origin", e.clientX + 'px ' + e.clientY + 'px');
    const id = this.props.id;
    this.setState({
      onTempClick: true
    })
    setTimeout(() => {
      this.setState({
        onTempClick: false
      })
    }, 5000);
    this.props.onHoverDetails(this.props.scene, true);
    this.props.onHoverComplete(true);
    this.setState({
      onClick: true
    })
  }

  _animateCircle = (percentage) => {
    const { circle } = this.props;

    circle.animate(percentage, {
      duration: percentage ? 2000 : 500
    }, function() {
      // circle.animate(0, {
      //   duration: 1000
      // }, function() {
        
      // });
    });
  }

  _onHold = (e) => {
    this.timer = setInterval(this._tick, 250);
    this._animateCircle(1);
    this.setState({
      onHold: true,
      posX: e.clientX,
      posY: e.clientY,
    });
    $('#dark-room').css("transform-origin", posX + 'px ' + posY + 'px');
    // this._animateCircle();
  }

  _tick = () => {
    this.setState(prevState => ({
      timing: prevState.timing + 250,
    }), () => {
      const { timing } = this.state;

      console.log(timing);

      if (timing == 2250) {
        const { scene, onHoverDetails, onHoverComplete } = this.props;
        const { posX, posY } = this.state;
        this.setState({ onTempClick: true })
        setTimeout(() => {
          this.setState({ onTempClick: false })
        }, 5000);
        onHoverDetails(scene, true);
        onHoverComplete(true);
        this.setState({ onComplete: true })
      }
    });
  }

  _onRelease = () => {
    console.log("On Release");
    clearInterval(this.timer);
    this._animateCircle(0);
    this.setState({
      onHold: false,
      timing: 0,
    });
  }

  _onMouseEnter = (e) => {
    this._onRelease();

    // this.setState({
    //   onHover: true,
    // });

    // this.props.circle.animate(1, { 
    //   duration: 2000,
    // }, () => {
    //   this.props.onHoverComplete(true);
    // });
  }

  _onMouseLeave = () => {
    this._onRelease();

    // this.props.onHoverIconCallback(35);
    // this.props.onHoverDetails(this.props.scene, false);

    // this.setState({
    //   onHover: false,
    // })

    // this.props.circle.animate(0, { duration: 500 });
    // this.setState({
    //   onHover: false,
    // })
  }


  render() {
    const { onClick, onComplete, onHover, onTempClick } = this.state;
    const { icon, id } = this.props;

    let className;
    if (onHover) {
      className = onTempClick ? 'hover-btn on-hover on-click' : 'hover-btn on-hover';
    } else {
      className = onTempClick ? 'hover-btn on-click' : 'hover-btn';
    }

    return (
      <div
        id           = {id}
        className    = {className}
        onClick        = {(e) => this._onClick(e)}
        // onMouseDown  = {e => this._onHold(e)}
        // onMouseUp    = {() => this._onRelease()}
        // onMouseEnter = {(e) => this._onMouseEnter(e)}
        // onMouseLeave = {() => this._onMouseLeave()}
      >
        <div className={onClick || onComplete ? "icon-wrapper visited" : "icon-wrapper"}>
          <img
            className = "icon"
            src       = {icon} alt = ""/>
        </div>
      </div>
    );
  }
}

export default HoverButton;