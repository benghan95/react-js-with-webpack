import React, { Component } from 'react';

const quotes = [
  {
    message: 'In Malaysia, only 140 of the 12,987 cases of child sexual abuse reported to police between 2012 and July 2016 resulted in convictions.',
    title  : 'The Straits Time'
  },
  {
    message: 'Every choice we make on behalf of children today, impacts their tomorrows. With appropriate attention and guidance, children can flourish in safe and protected childhoods.',
    title  : 'UNICEF'
  },
  {
    message: "You might think your children can’t be that stupid, but remember this: Guys...will spend weeks or months online manipulating your child. If it doesn’t work out, he’s untraceable, so he’ll just move on to the next child.",
    title  : 'R.AGE'
  },
]

class Quotes extends Component {
  constructor(props) {
    super(props)
    this.state = {
      started: false,
    }
  }

  render() {
    return (
      <div>
        <h3>{quotes[this.props.quote - 1].message}</h3>
        <h5>{quotes[this.props.quote - 1].title}</h5>
      </div>
    )
  }
}

export default Quotes;