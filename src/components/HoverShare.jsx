import React, { Component } from 'react';
import anime from 'animejs';

class HoverButton extends Component {
  constructor(props) {
    super(props);
  }

  _share = (platform) => {
    switch(platform) {
      case 'facebook':
        window.open(`https://facebook.com/sharer/sharer.php?u=${encodeURI('https://digi-cybersafe.tribalddb.com.my/?scene=' + this.props.scene)}`)
        break;
      case 'twitter':
        window.open(`https://twitter.com/share?url=${encodeURI('https://digi-cybersafe.tribalddb.com.my/?scene=' + this.props.scene)}`)
        break;
      case 'linkedin':
        window.open(`https://www.linkedin.com/shareArticle?mini=true&url=${encodeURI('https://digi-cybersafe.tribalddb.com.my/?scene=' + this.props.scene)}`)
        break;
      case 'whatsapp':
        window.open(`whatsapp://send?text=${encodeURI('https://digi-cybersafe.tribalddb.com.my/?scene=' + this.props.scene)} Click%20in%20to%20learn%20how%20to%20stop%20predators%20and%20keep%20social%20media%20safe%20for%20our%20kids.`)
        break;
    }
  }


  render() {
    return (
      <div className="hover-share">
        <i className="fa fa-facebook" onClick={() => this._share('facebook')}></i>
        <i className="fa fa-twitter" onClick={() => this._share('twitter')}></i>
        {/* <i className="fa fa-linkedin" onClick={() => this._share('linkedin')}></i> */}
        <i className="fa fa-whatsapp" onClick={() => this._share('whatsapp')}></i>
      </div>
    );
  }
}

export default HoverButton;