import React, { Component } from 'react';
import axios from 'axios';
// import Snap from 'imports-loader?this=>window,fix=>module.exports=0!snapsvg/dist/snap.svg.js';
// import Snapsvg from 'snapsvg-cjs';
// import './../assets/vendor/SnapSVGAnimator/SnapSVGAnimator.js';
import json from './../assets/json/stop-sign-vertical.json';


const jsonfile = "./../assets/json/stop-sign-vertical.json";
const fps      = 30;
const width    = 100;
const height   = 100;

class StopSign extends Component {
  constructor(props) {
    super(props)
    this.state = {
      started: false,
      data: '',
      container: null,
    }
  }

  componentDidMount() {
    // axios.get(jsonfile)
    //   .then(response => {
    //     this.setState({ data: response.data })
    //   });
    this.setState({
      container: document.getElementById('stop-sign-vertical-container'),
    })
    // const container = document.getElementById('stop-sign-vertical-container');
  }

  getSVG() {
    if (this.state.container) {
      const container = this.state.container;
      const comp = new window.SVGAnim(json, width, height, fps);
      // console.log(SVG);
      // container.appendChild(SVG.s.node);
    }
  }

  // handle_AJAX_Complete = () => {
  //   if ( AJAX_req.readyState == 4 && AJAX_req.status == 200 ) {
  //     json_a = JSON.parse(AJAX_req.responseText);
  //     // comp = new SVGAnim(json, width, height, fps);
  //     var container_a = document.getElementById("stop-sign-vertical-container");
  //     var comp_a      = new window.SVGAnim(json_a,width,height,fps);
  //     comp_a.s.node.setAttribute('id','stop-sign-vertical');
  //     container_a.appendChild(comp_a.s.node);
  //   }
  // }

  AJAX_JSON_Req = ( url ) => {
    AJAX_req = new XMLHttpRequest();
    AJAX_req.open("GET", url, true);
    AJAX_req.setRequestHeader("Content-type", "application/json");

    AJAX_req.onreadystatechange = this.handle_AJAX_Complete;
    AJAX_req.send();
  }

  render() {
    return (
      <div id="stop-sign-vertical-container">
        { this.state.container ? this.getSVG() : null }
      </div>
    )
  }
}

export default StopSign;