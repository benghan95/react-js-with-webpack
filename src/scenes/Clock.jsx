import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 2000)
  };


  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#clock-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/(\b[^\s]+|-+)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.setState({ videoPlaying: true });
          this.setState({ audioPlaying: true });
        }, 1500);

        setTimeout(() => {
          this.setState({ audioPlaying: false });
          this.audioPlayer.seekTo(0);
        }, 5250);

        var max_number = 15;

        var padding_zeros = '';
        for(var i = 0, l = max_number.toString().length; i < l; i++) {
          padding_zeros += '0';
        }

        var padded_now, numberStep = function(now, tween) {
          var target = $(tween.elem),
              rounded_now = Math.round(now);

          var rounded_now_string = rounded_now.toString()
          padded_now = padding_zeros + rounded_now_string;
          padded_now = padded_now.substring(rounded_now_string.length);

          target.prop('number', rounded_now).text(padded_now);
        };

        // setTimeout(() => {
        //   $('#clock-counter').animateNumber({
        //     number    : max_number,
        //     numberStep: numberStep,
        //     easing    : 'easeInOutExpo',
        //   }, 4000);
        // }, 3000);

        let clockAnime = anime.timeline({})
        .add({
          targets   : '#clock-scene .legend-raped',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
        })
        .add({
          targets   : '#clock-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets   : '#clock-scene .timer',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '-=1000'
        })
        .add({
          targets : '#clock-scene .legend-under-16',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
          offset  : '+=1500'
        })
        .add({
          targets : '#clock-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#clock-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "clock-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div className="col-6 offset-6 d-flex justify-content-start align-items-center">
            <div className="description scene-value">
              <h4 className="desc">
                In Malaysia, a woman is raped every
              </h4>
              <div>
                <h2 className="timer">15 minutes</h2>
                {/* <h2 className="timer">00:<span id="clock-counter">00</span>:00</h2> */}
              </div>

              <div className="bottom-info">
                <div className="legend">
                  <div className="legend-card legend-raped">
                    <img src={require('./../assets/images/legend/clock-pink.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Raped women</h6>
                  </div>

                  <div className="legend-card legend-under-16">
                    <img src={require('./../assets/images/legend/clock-red.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Victims under 16 years old</h6>
                  </div>
                </div>

                <div className="cta-buttons button-list vertical-buttons">
                  <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                    <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
                  </a>
                  <a href="#" className="btn btn-default btn-sm btn-share">
                    <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                    <HoverShare scene='clock'/>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;