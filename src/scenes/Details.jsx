import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import Bed from './Bed';
import Board from './Board';
import Bands from './Bands';
import Cabinet from './Cabinet';
import Shelves from './Shelves';
import Clock from './Clock';
import Lego from './Lego';
import Fan from './Fan';
import Trophy from './Trophy';

class RoomDetails extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  };

  render() {
    switch (this.props.scene){
      case 'bed':
        return (
          <Bed
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
            muted        = {this.props.muted}
            detailVolume = {this.props.detailVolume}
            bedVideo     = {this.props.bedVideo}
            bedAudio     = {this.props.bedAudio}
          />
        );
      case 'board':
        return (
          <Board
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
            muted        = {this.props.muted}
            detailVolume = {this.props.detailVolume}
            boardVideo   = {this.props.boardVideo}
            boardAudio   = {this.props.boardAudio}
          />
        );
      case 'bands':
        return (
          <Bands 
            focused              = {this.props.focused}
            detailsClass         = {this.props.detailsClass}
            muted                = {this.props.muted}
            detailVolume         = {this.props.detailVolume}
            friendshipbandsVideo = {this.props.friendshipbandsVideo}
            friendshipbandsAudio = {this.props.friendshipbandsAudio}
          />
        );
      case 'cabinet':
        return (
          <Cabinet
            focused       = {this.props.focused}
            detailsClass  = {this.props.detailsClass}
            muted         = {this.props.muted}
            detailVolume  = {this.props.detailVolume}
            wardrobeVideo = {this.props.wardrobeVideo}
            wardrobeAudio = {this.props.wardrobeAudio}
          />
        );
      case 'shelves':
        return (
          <Shelves
            focused             = {this.props.focused}
            detailsClass        = {this.props.detailsClass}
            muted               = {this.props.muted}
            detailVolume        = {this.props.detailVolume}
            stackedClothesVideo = {this.props.stackedClothesVideo}
            stackedClothesAudio = {this.props.stackedClothesAudio}
          />
        );
      case 'clock':
        return (
          <Clock
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
            muted        = {this.props.muted}
            detailVolume = {this.props.detailVolume}
            clockVideo   = {this.props.clockVideo}
            clockAudio   = {this.props.clockAudio}
          />
        );
      case 'lego':
        return (
          <Lego
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
            muted        = {this.props.muted}
            detailVolume = {this.props.detailVolume}
            legoVideo    = {this.props.legoVideo}
            legoAudio    = {this.props.legoAudio}
          />
        );
      case 'fan':
        return (
          <Fan
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
            muted        = {this.props.muted}
            detailVolume = {this.props.detailVolume}
            fanVideo     = {this.props.fanVideo}
            fanEndVideo  = {this.props.fanEndVideo}
            fanAudio     = {this.props.fanAudio}
          />
        );
      case 'trophy':
        return (
          <Trophy
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
            muted        = {this.props.muted}
            detailVolume = {this.props.detailVolume}
            trophyVideo  = {this.props.trophyVideo}
            trophyAudio  = {this.props.trophyAudio}
          />
        );
      default:
        return (
          null
        );
    };
    // return (
    //   <section
    //     className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}>
    //     <div className="bg-overlay"></div>
    //     <video
    //       ref = "video"
    //       src = {require('./../assets/images/stop-motion/06_Bed.mp4')}
    //     ></video>
    //     <div className="row">
    //       <div className="col-md-7 text-center left">
    //         <div className="left-info">
    //           <div className="text-center">
    //             <h2><span id="bed-counter">0</span><span className="small">%</span></h2>
    //             <h5 className={this.state.done ? '' : 'd-none'}>of these cases involved rape.</h5>
    //           </div>
    //         </div>
    //       </div>
    //       <div className="col-md-5 right">
    //         <div className="right-info">
    //           <div className="stats">
    //             <h3><span id="side-counter">22,134</span></h3>
    //             <h4>Malaysian children were sexually abused<br />between 2010 and 2017.</h4>
    //             <div className="button-list">
    //               <a href="#" className="btn btn-default">Learn How to Stop Them</a>
    //               <a href="#" className="btn btn-default btn-sm btn-share">
    //                 <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
    //               </a>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //   </section>
    // );
  }
}

export default RoomDetails;