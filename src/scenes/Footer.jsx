import React, { Component } from 'react';
import ReactPlayer from 'react-player';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      source  : null,
      playing : false,
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.focusedScene != this.props.focusedScene) {
      if (nextProps.focusedScene == 'bed') {
        this.setState({
          source: "MalayMail Online"
        })
      }
      if (nextProps.focusedScene == 'fan') {
        this.setState({
          source: "R.AGE Predator in My Phone"
        })
      }
      if (nextProps.focusedScene == 'trophy') {
        this.setState({
          source: "The Star Online "
        })
      }
      if (nextProps.focusedScene == 'board') {
        this.setState({
          source: "R.AGE Predator in My Phone"
        })
      }
      if (nextProps.focusedScene == 'bands') {
        this.setState({
          source: "R.AGE Predator in My Phone"
        })
      }
      if (nextProps.focusedScene == 'shelves') {
        this.setState({
          source: "Reuters"
        })
      }
      if (nextProps.focusedScene == 'cabinet') {
        this.setState({
          source: "R.AGE Predator in My Phone"
        })
      }
      if (nextProps.focusedScene == 'clock') {
        this.setState({
          source: "The Edge Markets"
        })
      }
      if (nextProps.focusedScene == 'lego') {
        this.setState({
          source: "Protect and Save The Children"
        })
      }
    }
  };

  _onClick = () => {
    this.player.seekTo(0);
    this.setState({
      playing: true,
    });
  }

  _closeModal = () => {
    this.player.seekTo(0);
    this.setState({
      playing: false,
    });
  }

  _videoEnded = () => {
    $('#video-modal').trigger('click');
    this.setState({
      playing: false,
    });
    this.player.seekTo(0);
  }

  render() {
    const { className, focused, isEnglish, onToggleMute, onToggleLanguage, muted } = this.props;
    const { source } = this.state;

    return (
      <section id="footer" className={className}>
        <div className="footer-left">
          <p className="source">
            {
              this.state.source ? `Source: ${this.state.source}` : null
            }
          </p>
        </div>
        <div className="footer-right">
          <div className={focused ? 'hidden footer-actions' : 'footer-actions'}>
            {/* <a id="language-icon" className="footer-action" href="#" onClick={() => onToggleLanguage()}>
              <span>
                {
                  isEnglish ? 'EN' : 'BM'
                }
              </span>
            </a> */}
            <span id="headphone-icon" className="footer-action" href="#">
              <input
                id='muted'
                type='checkbox'
                checked={muted}
                onChange={() => onToggleMute()}
              />
              <label htmlFor='muted'></label>
            </span>
            <a id="share-icon" className="footer-action" href="#">
              <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
            </a>
            <button id="watch-icon" className="footer-action btn btn-link" data-toggle="modal" data-target="#video-modal" onClick={() => this._onClick()}>
              <img src={require('./../assets/images/footer-watch.svg')} alt="Digi Predator Watch Icon"/>
            </button>
          </div>

          <div className="footer-owner">
            <div className="project-by">
              <p>A project by </p>
              <a id="" className="digi-logo" href="https://digi.my" target="_blank">
                <img src={require('./../assets/images/digi-logo.svg')} alt="Digi Logo"/>
              </a>
              <a id="" className="yellow-heart" href="https://digi.my/yellowheart" target="_blank">
                <img src={require('./../assets/images/digi-yellow-heart.svg')} alt="Digi Yellow Heart Logo"/>
              </a>
            </div>
          </div>
        </div>
        <div className="modal modal-video fade" id="video-modal" tabIndex="-1" role="dialog" aria-labelledby="videoModalLabel" onClick={() => this._closeModal()}>
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header justify-content-end">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => this._closeModal()}>
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div id="mobile-frame">
                  <img src={require('./../assets/images/vertical-phone.svg')} alt=""/>
                </div>
                <div className="embed-responsive embed-responsive-16by9">
                  <ReactPlayer
                    className = "footer-player"
                    ref       = {(c) => this.player = c}
                    url       = 'https://www.youtube.com/embed/gTahJpMJK7w'
                    config    = {{
                      youtube: {
                        playerVars: {
                          showinfo      : 0,
                          controls      : 0,
                          rel           : 0,
                          playsinline   : 1,
                          loop          : 0,
                          modestbranding: 1,
                        }
                      },
                    }}
                    loop    = {false}
                    playing = {this.state.playing}
                    onEnded = {() => this._videoEnded()}
                  />
                  {/* <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/gTahJpMJK7w"></iframe> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Footer;