import React, { Component } from 'react';
import anime from 'animejs';
import Menu from './Menu';
import Preloader from './Preloader';
import DarkRoom from './DarkRoom';
import Details from './Details';
import ProgressBar from 'progressbar.js';
import Room from './Room';
import Header from './Header';
import Footer from './Footer';
import { isBrowser, isMobile, isSafari } from "react-device-detect";

const BedVideo           = require('./../assets/images/stop-motion/bed.mp4');
const BoardVideo         = require('./../assets/images/stop-motion/board.mp4');
const ClockVideo         = require('./../assets/images/stop-motion/clock.mp4');
const FanEndVideo        = require('./../assets/images/stop-motion/fan-end.mp4');
const FanVideo           = require('./../assets/images/stop-motion/fan.mp4');
const BandsVideo         = require('./../assets/images/stop-motion/friendshipbands.mp4');
const LegoVideo          = require('./../assets/images/stop-motion/lego.mp4');
const ShelvesVideo       = require('./../assets/images/stop-motion/stacked-clothes.mp4');
const TrophyVideo        = require('./../assets/images/stop-motion/trophy.mp4');
const CabinetVideo       = require('./../assets/images/stop-motion/wardrobe.mp4');
const BedMobileVideo     = require('./../assets/images/stop-motion/bed-mobile.mp4');
const BoardMobileVideo   = require('./../assets/images/stop-motion/board-mobile.mp4');
const ClockMobileVideo   = require('./../assets/images/stop-motion/clock-mobile.mp4');
const FanEndMobileVideo  = require('./../assets/images/stop-motion/fan-end-mobile.mp4');
const FanMobileVideo     = require('./../assets/images/stop-motion/fan-mobile.mp4');
const BandsMobileVideo   = require('./../assets/images/stop-motion/friendshipbands-mobile.mp4');
const LegoMobileVideo    = require('./../assets/images/stop-motion/lego-mobile.mp4');
const ShelvesMobileVideo = require('./../assets/images/stop-motion/stacked-clothes-mobile.mp4');
const TrophyMobileVideo  = require('./../assets/images/stop-motion/trophy-mobile.mp4');
const CabinetMobileVideo = require('./../assets/images/stop-motion/wardrobe-mobile.mp4');

const BedAudio          = require('./../assets/audio/Detail-Bed.mp3');
const BoardAudio        = require('./../assets/audio/Detail-Board.mp3');
const ShelvesAudio      = require('./../assets/audio/Detail-Cabinet.mp3');
const ClockAudio        = require('./../assets/audio/Detail-Clock.mp3');
const BandsAudio        = require('./../assets/audio/Detail-Drawer.mp3');
const FanAudio          = require('./../assets/audio/Detail-Fan.mp3');
const LegoAudio         = require('./../assets/audio/Detail-Lego.mp3');
const TrophiesAudio     = require('./../assets/audio/Detail-Trophies.mp3');
const CabinetAudio      = require('./../assets/audio/Detail-Wardrobe.mp3');
const Lullaby           = require('./../assets/audio/Lullaby.mp3');
const DayAmbienceSoft   = require('./../assets/audio/Day-Ambience-Soft.mp3');
const NightAmbienceSoft = require('./../assets/audio/Night-Ambience-Soft.mp3');

import Bed from './Bed';
import Board from './Board';
import Bands from './Bands';
import Cabinet from './Cabinet';
import Shelves from './Shelves';
import Clock from './Clock';
import Lego from './Lego';
import Fan from './Fan';
import Trophy from './Trophy';

let circle    = null;
let moveForce = 10;    // max popup movement in pixels
let itemForce = 2;     // max popup movement in pixels

class Home extends Component {
	constructor (props){
		super(props);
		this.state = {
			width       : 0,
			height      : 0,
			focused     : false,
			focusedScene: null,
			// focused       : true,
			// focusedScene  : 'trophy',
			didMount: false,
			circle  : null,
			// passPreloader : true,
			// preloaderEnded: true,
			isEnglish     : true,
			passPreloader : false,
			preloaderEnded: false,
			menuOpen      : false,
			onButton      : false,
			cursorSize    : '35px',
			zoomDark      : false,
			muted         : true,
			roomVolume    : 0.05,
			detailVolume  : 0,
			// zoomDark: true,
		}
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}

	componentDidMount(){
		circle = new ProgressBar.Circle('#cursor', {
			strokeWidth: 10,
			easing     : 'easeInOut',
			duration   : 1400,
			color      : '#ed1b24',
			trailColor : '#fff',
			trailWidth : 5,
			svgStyle   : {
					width: '100%',
			}
		});

		if (!isSafari) {
			this.setState({ muted: false });
		}

		this.setState({
			circle  : circle,
			didMount: true,
		})

		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
			window.removeEventListener('resize', this.updateWindowDimensions);
	}

	updateWindowDimensions() {
		this.setState({
			width : window.innerWidth,
			height: window.innerHeight
		});
	}

	_onPassPreloader = (passPreloader) => {
		this.setState({ passPreloader: passPreloader });
	}

	_preloaderCallback = (preloaderEnded) => {
		this.setState({
			preloaderEnded: preloaderEnded,
			zoomDark: true,
		});

		if (this.state.preloaderEnded) {
			setTimeout(function () {
				this.setState({
					roomVolume: 0.2,
				})
			}.bind(this), 250);
			setTimeout(function () {
				this.setState({
					roomVolume: 0.4,
				})
			}.bind(this), 500);
			setTimeout(function () {
				this.setState({
					roomVolume: 0.6,
				})
			}.bind(this), 750);
			setTimeout(function () {
				this.setState({
					roomVolume: 0.8,
				})
			}.bind(this), 1000);
		}
	}

	_onHoverDetails = (scene, status) => {
		if (status){
			this.setState({
				focusedScene: scene
			})
		}
	}

	// onHoverIconCallback = (size) => {
	// 	const cursorSize = size + 'px';
	// 	this.setState(prevState => ({
	// 		cursorSize : cursorSize,
	// 		cursorStyle: {
	// 			opacity: prevState.cursorStyle.opacity,
	// 			left   : prevState.cursorStyle.left,
	// 			top    : prevState.cursorStyle.top,
	// 			width  : cursorSize,
	// 		}
	// 	}));
	// }

	_onHoverComplete = () => {
		// this.setState({
		// 	roomVolume: 0,
		// 	detailVolume: 0.35,
		// })
		setTimeout(function () {
			this.setState({
				roomVolume: 0.75,
				detailVolume: 0.0875,
			})
		}.bind(this), 250);
		setTimeout(function () {
			this.setState({
				roomVolume: 0.7,
				detailVolume: 0.175,
			})
		}.bind(this), 500);
		setTimeout(function () {
			this.setState({
				roomVolume: 0.65,
				detailVolume: 0.2625,
			})
		}.bind(this), 750);
		setTimeout(function () {
			this.setState({
				roomVolume: 0.6,
				detailVolume: 0.35,
			})
		}.bind(this), 1000);
	}

	onReturnRoom = (state) => {
		if (state){
			let returnRoomAnime = anime.timeline({})
			.add({
					targets : '.room-details.targetted',
					scale   : '1',
					easing  : "easeInCirc",
					duration: 1000,
			})
			.add({
					targets : '#return-room',
					opacity : [1, 0],
					easing  : "easeInExpo",
					duration: 500,
					offset  : '-=250'
			})
			.add({
					targets : '.room-details.targetted',
					opacity : [1, 0],
					easing  : "easeInCirc",
					duration: 500,
					offset  : '-=500'
			})
			;

			setTimeout(function () {
				this.setState({
					roomVolume: 0.6,
					detailVolume: 0.2625,
				})
			}.bind(this), 250);
			setTimeout(function () {
				this.setState({
					roomVolume: 0.65,
					detailVolume: 0.175,
				})
			}.bind(this), 500);
			setTimeout(function () {
				this.setState({
					roomVolume: 0.7,
					detailVolume: 0.0875,
				})
			}.bind(this), 750);
			setTimeout(function () {
				this.setState({
					roomVolume: 0.8,
					detailVolume: 0,
				})
			}.bind(this), 1000);

			returnRoomAnime.complete = () => {
				this.setState({
					focused: false,
				});
				this.onZoom(false);
				anime.timeline({})
				.add({
						targets : '#menu',
						opacity : [0, 1],
						easing  : "easeInExpo",
						duration: 500,
				})
				.add({
						targets : '.room-details.targetted',
						scale   : '1',
						opacity : 1,
						easing  : "easeInExpo",
						duration: 0,
				})
				.add({
						targets : '#footer .footer-left',
						opacity : 0,
						easing  : "easeInExpo",
						duration: 0,
						offset  : '-=500'
				})
			}
		}
	}

	onToggleMenuBtn = () => {
		this.setState((prevState) => { return {
				menuOpen: !prevState.menuOpen
		}});
	}

	onButtonCallback = (onButton) => {
		this.setState({ onButton: onButton });
	}

	_onMouseMove = (e) => {
		e.persist();
		this.setState({
			cursorStyle: {
				left: e.pageX + 'px',
				top : e.pageY + 'px',
			},
		});

		// if (this.state.zoomDark){
		// 	var docX = this.state.width;
		// 	var docY = this.state.height;

		// 	var moveX = 50 + (e.pageX - docX/2) / (docX/2) * moveForce;
		// 	var moveY = 50 + (e.pageY - docY/2) / (docY/2) * moveForce;

		// 	// console.log("PageX: " + moveX);
		// 	// console.log("PageY: " + moveY);

		// 	var itemX = 50 + (e.pageX - docX/2) / (docX/2) * -itemForce;
		// 	var itemY = 50 + (e.pageY - docY/2) / (docY/2) * -itemForce;

		// 	this.setState({
		// 			roomStyle: {
		// 					backgroundPosition: moveX + '% ' + moveY + '%',
		// 			},
		// 			roomItemsStyle: {
		// 					left: itemX + '%',
		// 					top : itemY + '%',
		// 			},
		// 			quoteStyle: {
		// 					// left: (6 +  moveX) + '%',
		// 					// top : (69 + moveY) + '%',
		// 			},
		// 	});
		// }
	}

	onZoom = (zoomed) => {
		if(zoomed) {
			this.setState({
				focused: true,
			})
		} else {
			this.setState({
				focused: false,
			})
		}
	}

	_unmuteSound = (unmute) => {
		if (unmute) {
			this.setState({
				muted: false
			})
		}
	}

	_toggleMute = () => {
		this.setState((prevState) => { return {
			muted: !prevState.muted
		}});
	}

	_toggleLanguage = () => {
		this.setState((prevState) => { return {
			isEnglish: !prevState.isEnglish
		}});
	}

	// _handleVideoLoaded = (value) => {
	// 	console.log(value);
	// }

	// _handleAudioLoaded = (value) => {
	// 	console.log(value);
	// }

	render() {
		const { preloaderEnded, focused, focusedScene, menuOpen, circle, muted, detailsClass, isEnglish, detailVolume, didMount, zoomDark, roomVolume } = this.state;
		return (
			<main
				id="page-home"
				onMouseMove = {e => this._onMouseMove(e)}
			>
				<div id="cursor" style={this.state.cursorStyle}></div>
				<Header
					className       = {(preloaderEnded) ? '' : 'd-none' }
					focused         = {focused}
					focusedScene    = {focusedScene}
					ticketId        = {1}
					menuOpen        = {menuOpen}
					onToggleMenuBtn = {this.onToggleMenuBtn}
					onReturnRoom    = {this.onReturnRoom}
					onZoom          = {this.onZoom}
					muted           = {muted}
				/>
				<Menu menuOpen = {menuOpen}/>
				<Footer
					className        = {(preloaderEnded) ? '' : 'd-none' }
					focusedScene     = {focusedScene}
					focused          = {focused}
					muted            = {muted}
					isEnglish        = {isEnglish}
					onToggleLanguage = {this._toggleLanguage}
					onToggleMute     = {this._toggleMute}
				/>
				{
					preloaderEnded ? null :
					<Preloader
						mounted           = {didMount}
						onPassPreloader   = {this._onPassPreloader}
						preloaderCallback = {this._preloaderCallback}
						muted             = {muted}
						screenWarning     = {this.props.screenWarning}
						unmuteSound       = {this._unmuteSound}
					/>
				}

				<DarkRoom
					focused                = {focused}
					reached                = {zoomDark}
					circle                 = {circle}
					onHoverDetails         = {this._onHoverDetails}
					onHoverComplete        = {this._onHoverComplete}
					onZoom                 = {this.onZoom}
					muted                  = {muted}
					roomVolume             = {roomVolume}
					lullabyAudio           = {Lullaby}
					dayAmbienceSoftAudio   = {DayAmbienceSoft}
					nightAmbienceSoftAudio = {NightAmbienceSoft}
				/>

				<Bed
					focused      = {focusedScene === 'bed' ? focused : false}
					targetted    = {focusedScene === 'bed' ? true : false}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? BedVideo : BedMobileVideo}
					audio        = {BedAudio}
				/>
				<Board
					focused      = {focusedScene === 'board' ? focused : false}
					targetted    = {focusedScene === 'board' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? BoardVideo : BoardMobileVideo}
					audio        = {BoardAudio}
				/>
				<Clock
					focused      = {focusedScene === 'clock' ? focused : false}
					targetted    = {focusedScene === 'clock' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? ClockVideo : ClockMobileVideo}
					audio        = {ClockAudio}
				/>
				<Fan
					focused      = {focusedScene === 'fan' ? focused : false}
					targetted    = {focusedScene === 'fan' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					endVideo     = {isBrowser ? FanEndVideo : FanEndMobileVideo}
					video        = {isBrowser ? FanVideo : FanMobileVideo}
					audio        = {FanAudio}
				/>
				<Bands
					focused      = {focusedScene === 'bands' ? focused : false}
					targetted    = {focusedScene === 'bands' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? BandsVideo : BandsMobileVideo}
					audio        = {BandsAudio}
				/>
				<Lego
					focused      = {focusedScene === 'lego' ? focused : false}
					targetted    = {focusedScene === 'lego' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? LegoVideo : LegoMobileVideo}
					audio        = {LegoAudio}
				/>
				<Shelves
					focused      = {focusedScene === 'shelves' ? focused : false}
					targetted    = {focusedScene === 'shelves' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? ShelvesVideo : ShelvesMobileVideo}
					audio        = {ShelvesAudio}
				/>
				<Trophy
					focused      = {focusedScene === 'trophy' ? focused : false}
					targetted    = {focusedScene === 'trophy' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? TrophyVideo : TrophyMobileVideo}
					audio        = {TrophiesAudio}
				/>
				<Cabinet
					focused      = {focusedScene === 'cabinet' ? focused : false}
					targetted    = {focusedScene === 'cabinet' ? 'targetted' : ''}
					detailsClass = {detailsClass}
					muted        = {muted}
					detailVolume = {detailVolume}
					video        = {isBrowser ? CabinetVideo : CabinetMobileVideo}
					audio        = {CabinetAudio}
				/>
			</main>
		);
	}
}

export default Home;