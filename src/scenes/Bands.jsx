import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#bands-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.setState({ videoPlaying: true });
          this.setState({ audioPlaying: true });
        }, 1000);

        setTimeout(() => {
          $('#bands-value').animateNumber({
            number: 80,
            easing: 'easeInOutExpo',
          }, 1500);
        }, 1200);

        let bandsAnime = anime.timeline({})
        .add({
          targets : '#bands-scene .legend',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        .add({
          targets   : '#bands-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '-=500',
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets   : '#bands-bottom-line',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
          offset    : '+=1500'
        })
        .add({
          targets   : '#bands-scene .total-rape-cases',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          // offset    : '+=1000'
        })
        .add({
          targets : '#bands-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#bands-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        $('#bands-value').html('0');
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "bands-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref         = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className   = 'video-player'
          url         = {this.props.video}
          playing     = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="content">
          <div className="total-rape-cases">
            <div id="bands-top-line">
              <img src={require('./../assets/images/line-bands-top.png')} alt=""/>
            </div>
            <h4>Total rape cases</h4>
          </div>
          <div className="scene-value">
            <div className="left-info">
              <div className="description">
                <div id="bands-bottom-line">
                  <img src={require('./../assets/images/line-bands-bottom.png')} alt=""/>
                </div>
                <h2 className="percentage"><span id="bands-value">0</span>%</h2>
                <h4 className="desc">of rape cases begin as an online friendship.</h4>
              </div>
              <div className="legend vertical-legend">
                <div className="legend-card">
                  <img src={require('./../assets/images/legend/bands-red.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">Internet Acquaintances</h6>
                </div>

                <div className="legend-card">
                  <img src={require('./../assets/images/legend/bands-white.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">Others</h6>
                </div>
              </div>
            </div>
            <div className="cta-buttons button-list">
              <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
              </a>
              <a href="#" className="btn btn-default btn-sm btn-share">
                <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                <HoverShare scene='bands'/>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;