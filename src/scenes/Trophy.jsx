import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 3000);
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        setTimeout(() => {
          this.setState({ videoPlaying: true });

          $('#trophy-scene .rank.third h3').each(function(){
            // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
            $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
          });
          $('#trophy-scene .desc').each(function(){
            // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
            $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
          });

          setTimeout(() => {
            this.setState({ audioPlaying: true });
          }, 2500);

          let trophyAnime = anime.timeline({})
          .add({
            targets : '#trophy-scene .rank.third',
            opacity : [0,1],
            easing  : "easeOutCirc",
            duration: 0,
            offset  : 0,
          })
          .add({
            targets : '#trophy-scene .rank h3',
            opacity : [0,1],
            easing  : "easeOutCirc",
            duration: 0,
            offset  : 0,
          })
          .add({
            targets : '#trophy-scene .rank.third h2',
            scale   : [5,1],
            opacity : [0,1],
            easing  : "easeOutCirc",
            duration: 800,
            offset  : 2000,
          })
          .add({
              targets : '#trophy-scene .rank.first',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: 2000
          })
          .add({
              targets : '#trophy-scene .rank.second',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: 2000
          })
          .add({
              targets : '#trophy-scene .rank.third',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: 2000
          })
          .add({
              targets : '#trophy-scene .rank.forth',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: 2000
          })
          .add({
              targets : '#trophy-scene .rank.fifth',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: 2000
          })
          .add({
            targets   : '#trophy-scene .rank.third h3 .letter',
            translateX: [40,0],
            translateZ: 0,
            opacity   : [0,1],
            easing    : "easeOutExpo",
            duration  : 1000,
            delay     : function(el, i) {
              return 100 + 20 * i;
            }
          })
          .add({
            targets   : '#trophy-scene .desc .letter',
            translateX: [40,0],
            translateZ: 0,
            opacity   : [0,1],
            easing    : "easeOutExpo",
            duration  : 1000,
            delay     : function(el, i) {
              return 100 + 20 * i;
            }
          })
          .add({
            targets : '#trophy-scene .btn-learn-stop',
            opacity : [0,1],
            easing  : "easeOutExpo",
            duration: 500,
            offset  : '+=500',
          })
          .add({
            targets : '#trophy-scene .btn-share',
            opacity : [0,1],
            easing  : "easeOutExpo",
            duration: 500,
          })
          .add({
            targets : '#footer .footer-left',
            opacity : [0,1],
            easing  : "easeOutExpo",
            duration: 500,
          })
          .add({
            targets : '#return-room',
            opacity : [0,1],
            easing  : "easeOutExpo",
            duration: 500,
            offset  : '-=500',
          })
        }, 500);
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        let trophyAnime = anime.timeline({})
        .add({
          targets : '#trophy-scene .rank.third h2',
          scale   : 5,
          opacity : 0,
          duration: 0,
        })
        .add({
          targets : '#trophy-scene .rank',
          opacity : 0,
          duration: 0
        })
        .add({
          targets : '#trophy-scene .rank h3',
          opacity : 0,
          easing  : "easeOutCirc",
          duration: 0,
          offset  : 0,
        })
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "trophy-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div className="col-md-12">
            <div className="rank first">
              <h2>1<sup className="ordinal">st</sup></h2>
            </div>
            <div className="rank second">
              <h2>2<sup className="ordinal">nd</sup></h2>
            </div>
            <div className="rank third focus">
              <h3>Malaysia ranks</h3>
              <h2>3<sup className="ordinal">rd</sup></h2>
              <h3>in ASEAN for possession and</h3>
              <h3>distribution of child pornography. </h3>
              <h3>There are no specific laws against this.</h3>
            </div>
            <div className="rank forth">
              <h2>4<sup className="ordinal">th</sup></h2>
            </div>
            <div className="rank fifth">
              <h2>5<sup className="ordinal">th</sup></h2>
            </div>

            <div className="cta-buttons button-list vertical-buttons">
              <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
              </a>
              <a href="#" className="btn btn-default btn-sm btn-share">
                <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                <HoverShare scene='trophy'/>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;