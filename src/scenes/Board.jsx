import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import StopSignVertical from './../components/StopSignVertical';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#board-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.setState({ videoPlaying: true });
          this.setState({ audioPlaying: true });
        }, 1500);

        let boardAnime = anime.timeline({})
        .add({
          targets : '#board-scene .special-title',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset: '+=500'
        })
        .add({
          targets : '#board-scene .right-info h2',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500'
        })
        .add({
          targets   : '#board-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets : '#wechat-percentage',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
          offset  : '-=500'
        })
        .add({
          targets : '#messenger-percentage',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 750,
          offset  : '-=250'
        })
        .add({
          targets : '#beetalk-percentage',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#whatsapp-percentage',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=250',
        })
        .add({
          targets : '#board-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#board-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
      }
    }
  }

  render() {
    return (
      <section
        id        = "board-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {require('./../assets/audio/Detail-Board.mp3')}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div className="col-md-12 d-flex justify-content-center">
            <div className="description">
              <div className="scene-value">
                <div className="special-title">
                  <h1><span className="symbol">#</span>1</h1>
                </div>
                <div className="right-info">
                  <h2>modus operandi</h2>
                  <h4 className="desc">for sexual predators is via chat apps. WeChat is popular</h4>
                  <h4 className="desc">because it allows them to search for children nearby.</h4>
                </div>
              </div>

              <div className="cta-buttons button-list">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
                  {/* <StopSignVertical /> */}
                </a>
                <a href="#" className="btn btn-default btn-sm btn-share">
                  <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                  <HoverShare scene='board'/>
                </a>
              </div>
            </div>
          </div>

          {/* <div className="percentage-value">
            <div id="wechat-percentage" className="percentage digits">75<span className="percent">%</span></div>
            <div id="messenger-percentage" className="percentage digits">17<span className="percent">%</span></div>
            <div id="beetalk-percentage" className="percentage digits">3<span className="percent">%</span></div>
            <div id="whatsapp-percentage" className="percentage digits">3<span className="percent">%</span></div>
          </div> */}
        </div>
      </section>
    );
  }
}

export default Scene;