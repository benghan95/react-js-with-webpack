import React, { Component } from 'react';
import anime from 'animejs';
import ProgressBar from 'progressbar.js';
import particlesjs from 'particles.js';
import particles from './../assets/json/particles.json';
import { TweenMax, TextPlugin, TweenLite, TimelineMax } from "gsap";

let moveForce = 30;    // max popup movement in pixels
let itemForce = 12.5;  // max popup movement in pixels

class Room extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width         : 0,
      height        : 0,
      roomStyle     : {},
      roomItemsStyle: {},
      onHold        : false,
      canRelease    : false,
    }
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    particlesJS('particles-js', particles);
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({
      width : window.innerWidth,
      height: window.innerHeight
    });
  }

  _onMouseEnter = () => {
    this.props.onButtonCallback(true);
  }
  _onMouseLeave = () => {
    this.props.onButtonCallback(false);
  }

  _onMouseDown = () => {
    this.setState({
      onHold: true,
      roomStyle: {
        // transform : '',
        opacity   : '0',
        transition: 'all 2.0s cubic-bezier(0.95, 0.05, 0.795, 0.035)',
      }
    });

    if (this.props.circle){
      this.props.circle.animate(1, { duration: 2000 }, () => {
        this.setState({
          canRelease: true,
        });
        this.props.onHoverEnd(true);
      });
    }

  }

  _onMouseUp = () => {
    this.setState({
      onHold: false,
    })
    if (this.props.circle){
      if (this.state.canRelease){
        this.props.circle.animate(0, { duration: 500 });
        this.setState({
          roomClass: 'd-none',
        })
      }
      else{
        this.props.circle.animate(0, { duration: 500 });
        this.setState({
          roomStyle: {
            // transform : 'scale(1)',
            // opacity   : '1',
            // transition: 'all 1s ease',
          }
        })
        // let backBright = new TimelineMax();
        // backBright
        //   .set($('#room'), { opacity: 1 }, 1);
      }
    }
  }

  render() {
    return (
      <section
        id           = "room"
        className    = {[this.state.roomClass]}
        style        = {this.state.roomStyle}
        onMouseDown  = {() => this._onMouseDown()}
        onMouseUp    = {() => this._onMouseUp() }
        onMouseLeave = {() => this._onMouseUp() }
      >
        <div id="particles-js"></div>
        <img id="light-ray" src={require('./../assets/images/sunlight.png')} alt=""/>
        <img id="bed-light" src={require('./../assets/images/bed-sunlight.png')} alt=""/>
        <img id="floor-light" src={require('./../assets/images/floor-sunlight.png')} alt=""/>
        {/* <div className="instruction">
          <div className="hold-icon">
            <div className="icon-core"></div>
          </div>
          <p><strong>Click & Hold</strong> to take a closer look</p>
        </div> */}
        <img src={require('./../assets/images/text-overlay.png')} alt="" id="text-overlay"/>
        <div className="quote">
          <h2>Every day, sexual crimes against<br />children occur hidden in plain sight.</h2>
          <h3>Click & Hold to take a closer look.</h3>
        </div>
      </section>
    );
  }
}

export default Room;