import React, { Component } from 'react';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onClick: false,
    }
    this.state = {
      value         : 0,
      detailIcon    : require('./../assets/images/icons/fan.svg'),
      detailTitle   : 'ILL WIND',
      detailSubtitle: 'Raise The Alarm',
    }
  }

  _onClick = () => {
    this.props.onReturnRoom(true);
    this.setState({
      onClick: true
    })
    setTimeout(() => {
      this.setState({
        onClick: false
      })
    }, 1000);
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.focusedScene != this.props.focusedScene) {
      if (nextProps.focusedScene == 'bed') {
        this.setState({
          detailIcon: require('./../assets/images/icons/bed.svg'),
          detailTitle: 'PILLOWTALK',
          detailSubtitle: "What's The Damage?",
        })
      }
      if (nextProps.focusedScene == 'fan') {
        this.setState({
          detailIcon: require('./../assets/images/icons/fan.svg'),
          detailTitle: 'ILL WIND',
          detailSubtitle: "Raise The Alarm",
        })
      }
      if (nextProps.focusedScene == 'trophy') {
        this.setState({
          detailIcon: require('./../assets/images/icons/trophy.svg'),
          detailTitle: 'CHAMPION',
          detailSubtitle: "A Trophy Of Shame",
        })
      }
      if (nextProps.focusedScene == 'board') {
        this.setState({
          detailIcon: require('./../assets/images/icons/board.svg'),
          detailTitle: 'CHITCHAT',
          detailSubtitle: "Their Modus Operandi",
        })
      }
      if (nextProps.focusedScene == 'bands') {
        this.setState({
          detailIcon: require('./../assets/images/icons/friendship-bands.svg'),
          detailTitle: 'WOVEN LIES',
          detailSubtitle: "Reality Bytes",
        })
      }
      if (nextProps.focusedScene == 'shelves') {
        this.setState({
          detailIcon: require('./../assets/images/icons/shelves.svg'),
          detailTitle: 'SKELETONS',
          detailSubtitle: "Hiding In Here",
        })
      }
      if (nextProps.focusedScene == 'cabinet') {
        this.setState({
          detailIcon: require('./../assets/images/icons/hanging-clothes.svg'),
          detailTitle: 'HUSH NOW',
          detailSubtitle: "No Child's Play",
        })
      }
      if (nextProps.focusedScene == 'clock') {
        this.setState({
          detailIcon: require('./../assets/images/icons/clock.svg'),
          detailTitle: 'TICKTOCK',
          detailSubtitle: "And On It Goes",
        })
      }
      if (nextProps.focusedScene == 'lego') {
        this.setState({
          detailIcon: require('./../assets/images/icons/lego.svg'),
          detailTitle: 'BLOCKS',
          detailSubtitle: "Relationship-Building",
        })
      }
    }
  };


  render() {
    return (
      <section id="header" className={[this.props.className]}>
        <div className="navbar-left navbar-top">
          {
            this.props.focused ?
            <div className="detail-header">
              <div className="detail-icon">
                <img
                  className = "icon"
                  src       = {this.state.detailIcon} alt = ""/>
              </div>
              <div className="detail-title">
                <h6>{this.state.detailSubtitle}</h6>
                <h5>{this.state.detailTitle}</h5>
              </div>
            </div>: 
            <a className="navbar-brand" href="#">
              <img src={require('./../assets/images/logo-pred-b4.svg')} alt="Digi Predator Logo"/>
            </a>
          }
        </div>
        { this.props.focused ?
          <div className="navbar-right navbar-top">
            <div
              id        = "return-room"
              className = {this.state.onClick ? 'on-close' : ''}
            >
              <div
                className = "icon-wrapper"
                onClick   = {() => this._onClick()}
              >
                <span className="lnr lnr-cross"></span>
              </div>
            </div>
          </div> :
          <div className="navbar-right navbar-top">
            <button
              id        = "menu-btn"
              className = { (this.props.menuOpen) ? "hamburger hamburger--slider is-active" : "hamburger hamburger--slider" }
              type      = "button"
              onClick   = { () => this.props.onToggleMenuBtn() }>
              <span className="hamburger-box">
                <span className="hamburger-inner"></span>
              </span>
            </button>
          </div>
        }
      </section>
    );
  }
}

export default Header;