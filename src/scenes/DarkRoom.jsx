import React, { Component } from 'react';
import anime from 'animejs';
import particlesjs from 'particles.js';
import particles from './../assets/json/particles.json';
import HoverButton from './../components/HoverButton';
import moment from 'moment';
import ReactPlayer from 'react-player';
import Parallax from 'parallax-js';
import { isBrowser, isMobile, isFirefox } from "react-device-detect";

let moveForce = 20;  // max popup movement in pixels
let itemForce = 2;   // max popup movement in pixels

let lightRayAnime;
let bedLightAnime;
let floorLightAnime;

class DarkRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width         : 0,
      height        : 0,
      roomClass     : '',
      astronomy     : 'night',
      playing       : false,
      lullabyVolume : 0,
      dayPlaying    : false,
      nightPlaying  : false,
      timing        : 0,
      canRelease    : false,
      roomItemsClass: null,
    }
    // this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  /**
  |--------------------------------------------------
  | Component functions
  |--------------------------------------------------
  */
  componentWillMount() {
    const now = moment();
    const hour = now.hour();
    // const hour = 21;

    if (hour > 7 && hour < 18)
      this.setState({ astronomy: 'day', nightPlaying: false, dayPlaying: true });
    else if ((hour >= 5 && hour <= 7) || (hour >= 18 && hour <= 20))
      this.setState({ astronomy: 'dusk', nightPlaying: true, dayPlaying: false });
    else if (hour > 20 || hour < 5)
      this.setState({ astronomy: 'night', nightPlaying: true, dayPlaying: false });
  }

  componentDidMount() {
    if (this.state.astronomy === 'day' && isBrowser) {
      particlesJS('particles-js', particles);
      lightRayAnime = anime.timeline({ loop: true })
      .add({
        targets : '#light-ray',
        scale   : 0.85,
        easing  : "easeInOutCubic",
        duration: 0,
      })
      .add({
        targets : '#light-ray',
        scale   : 1.1,
        easing  : "easeInOutCubic",
        duration: 6000,
      })
      .add({
        targets : '#light-ray',
        scale   : 0.85,
        easing  : "easeInOutCubic",
        duration: 6000,
      });

      bedLightAnime = anime.timeline({ loop: true })
      .add({
        targets : '#bed-light',
        scale   : 0.85,
        opacity : 0.7,
        easing  : "easeInOutCubic",
        duration: 0,
      })
      .add({
        targets : '#bed-light',
        scale   : 1.1,
        opacity : 1,
        easing  : "easeInOutCubic",
        duration: 6000,
      })
      .add({
        targets : '#bed-light',
        scale   : 0.85,
        opacity : 0.7,
        easing  : "easeInOutCubic",
        duration: 6000,
      });

      floorLightAnime = anime.timeline({ loop: true })
      .add({
        targets : '#floor-light',
        scale   : 0.85,
        opacity : 0.7,
        easing  : "easeInOutCubic",
        duration: 0,
      })
      .add({
        targets : '#floor-light',
        scale   : 1.1,
        opacity : 1,
        easing  : "easeInOutCubic",
        duration: 6000,
      })
      .add({
        targets : '#floor-light',
        scale   : 0.85,
        opacity : 0.7,
        easing  : "easeInOutCubic",
        duration: 6000,
      });

      lightRayAnime.pause();
      bedLightAnime.pause();
      floorLightAnime.pause();
    }

    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions.bind(this));
    const scene = document.getElementById('dark-room');
    if (!isFirefox) {
      const parallaxInstance = new Parallax(scene, {
        pointerEvents: true
      });
    }

    // this.bgVideo.play();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions.bind(this));
  }

  updateWindowDimensions() {
    this.setState({
      width : window.innerWidth,
      height: window.innerHeight
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.reached != this.props.reached){
      let hoverBtnAnime = anime.timeline({})
      .add({
        targets : '#dark-room .hover-btn',
        opacity : [0, 1],
        easing  : "easeOutBack",
        duration: 3000,
        duration: function(el, i, l) {
          return 500 + (i * 500);
        }
      });

      if (this.state.astronomy === 'day' && isBrowser) {
        lightRayAnime.play();
        bedLightAnime.play();
        floorLightAnime.play();
      }
    }

    if(prevProps.focused !== this.props.focused){
      if (this.props.focused) {
        if (this.state.astronomy === 'day' && isBrowser) {
          lightRayAnime.pause();
          bedLightAnime.pause();
          floorLightAnime.pause();
        }
      } else {
        if (this.state.astronomy === 'day' && isBrowser) {
          lightRayAnime.play();
          bedLightAnime.play();
          floorLightAnime.play();
        }
      }
    }
  }

  _onHoverComplete = (state) => {
    if (state){
      if (this.state.astronomy === 'day' && isBrowser) {
        lightRayAnime.pause();
        bedLightAnime.pause();
        floorLightAnime.pause();
      }

      let zoomDetailsAnime = anime.timeline({})
      .add({
        targets : '.room-details',
        scale   : '1',
        easing  : "easeInExpo",
        duration: 0,
      })
      .add({
        targets : '#dark-room',
        scale   : '1.3',
        easing  : "easeInExpo",
        duration: 1000,
      })
      .add({
        targets : '#dark-room',
        opacity : [1, 0],
        easing  : "easeInExpo",
        duration: 500,
        offset  : '-=500'
      })
      .add({
        targets : '#menu',
        opacity : [1, 0],
        easing  : "easeInExpo",
        duration: 500,
        offset  : '-=500'
      })

      this.props.onHoverComplete();

      zoomDetailsAnime.complete = () => {
        this.props.onZoom(true);
        anime.timeline({})
        .add({
          targets : '#dark-room',
          scale   : '1',
          opacity : 1,
          easing  : "ease",
          duration: 0,
        })
      }
    }
  }

  _onHoverEaster = (e) => {
    this.setState({
      playing: true,
    })
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.05,
      })
    }.bind(this), 250);
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.15,
      })
    }.bind(this), 500);
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.2,
      })
    }.bind(this), 750);
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.25,
      })
    }.bind(this), 1000);
  }

  _onLeaveEaster = () => {
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.2,
      })
    }.bind(this), 250);
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.15,
      })
    }.bind(this), 500);
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0.05,
      })
    }.bind(this), 750);
    setTimeout(function () {
      this.setState({
        lullabyVolume: 0,
        playing: false,
      })
    }.bind(this), 1000);
  }

  _animateCircle = (percentage) => {
    const { circle } = this.props;

    circle.animate(percentage, {
      duration: percentage ? 2000 : 500
    }, function() {
      circle.animate(0, {
        duration: 1000
      });
    });
  }

  _onHold = (e) => {
    const { canRelease } = this.state;
    if (!canRelease) {
      this.setState({
        roomItemsStyle: {
          opacity: 1,
        },
        onHold: true,
        roomItemsClass: 'visible',
      });
      this.timer = setInterval(this._tick, 250);
      this._animateCircle(1);
    }
    // this._animateCircle();
  }

  _tick = () => {
    this.setState(prevState => ({
      timing: prevState.timing + 250,
    }), () => {
      const { timing } = this.state;

      if (timing == 2250) {
        this.setState({
          canRelease: true,
        })
      }
    });
  }

  _onRelease = () => {
    const { canRelease } = this.state;
    clearInterval(this.timer);
    if (!canRelease) {
      this._animateCircle(0);
      this.setState({
        roomItemsClass: null,
        roomItemsStyle: {
          opacity: 0,
        }
      })
    } else {
      this.setState({
        roomItemsStyle: {
          opacity: 1,
        }
      })
    }

    this.setState({
      onHold: false,
      timing: 0,
    });
  }

  _onMouseEnter = (e) => {
    this._onRelease();
  }

  _onMouseLeave = () => {
    this._onRelease();
  }

  render() {
    const { muted, roomVolume, circle, focused } = this.props;
    const { roomItemsStyle, astronomy, roomItemsClass } = this.state;

    return (
      <section
        id        = "dark-room"
        className = {(!focused ? astronomy : `focused ${astronomy}`)}
        // onTouchMove = {e => this._onTouchMove(e)}
        // onMouseMove = {e => this._onMouseMove(e)}
        onTouchStart = {() => this._onHold()}
        onTouchEnd   = {() => this._onRelease()}
        onMouseDown  = {e => this._onHold(e)}
        onMouseUp    = {() => this._onRelease()}
        onMouseEnter = {(e) => this._onMouseEnter(e)}
        onMouseLeave = {() => this._onMouseLeave()}
      >
        <div
          data-depth="0.1"
          className = {`day-component ${astronomy === 'day' ? '' : 'd-none'}`}
        >
          <div id="particles-js"></div>
          <img id="light-ray" src={require('./../assets/images/sunlight.png')} alt=""/>
          <img id="bed-light" src={require('./../assets/images/bed-sunlight.png')} alt=""/>
          <img id="floor-light" src={require('./../assets/images/floor-sunlight.png')} alt=""/>
        </div>
        <div className="player-wrapper">
          <ReactPlayer
            ref         = {(c) => { this.dayPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className   = 'react-player'
            url         = {require('./../assets/audio/Day-Ambience-Soft.mp3')}
            muted       = {muted}
            volume      = {roomVolume ? roomVolume : 0.05}
            playing     = {this.state.dayPlaying}
            loop        = {true}
          />
          <ReactPlayer
            ref         = {(c) => { this.nightPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className   = 'react-player'
            url         = {require('./../assets/audio/Night-Ambience-Soft.mp3')}
            muted       = {muted}
            volume      = {roomVolume ? roomVolume : 0.05}
            playing     = {this.state.nightPlaying}
            loop        = {true}
          />
        </div>
        <div data-depth="0.1" id="room-fixed">
          <div className="quote">
            <h2>Every day, sexual crimes against<br />children occur hidden in plain sight.</h2>
            <h3>Click and hold to take a closer look.</h3>
          </div>
        </div>
        <section data-depth="0.5" id="data-points">
          <div
            id        = "room-items"
            className = {roomItemsClass}
            style     = {roomItemsStyle}
          >
            <div
              id           = "easter-egg"
              onMouseEnter = {(e) => this._onHoverEaster(e)}
              onMouseLeave = {() => this._onLeaveEaster()}
            >
              <ReactPlayer
                ref         = {(c) => { this.easterPlayer = c; }}
                playsinline = {true}
                controls    = {false}
                className   = 'react-player'
                url         = {this.props.lullabyAudio}
                muted       = {muted}
                volume      = {this.state.lullabyVolume}
                playing     = {this.state.playing}
                loop        = {true}
              />
            </div>
            <HoverButton
              id                  = "bed-icon"
              icon                = {require('./../assets/images/icons/bed.svg')}
              scene               = 'bed'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "bands-icon"
              icon                = {require('./../assets/images/icons/friendship-bands.svg')}
              scene               = 'bands'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "board-icon"
              icon                = {require('./../assets/images/icons/board.svg')}
              scene               = 'board'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "cabinet-icon"
              icon                = {require('./../assets/images/icons/hanging-clothes.svg')}
              scene               = 'cabinet'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "shelves-icon"
              icon                = {require('./../assets/images/icons/shelves.svg')}
              scene               = 'shelves'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "clock-icon"
              icon                = {require('./../assets/images/icons/clock.svg')}
              scene               = 'clock'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "lego-icon"
              icon                = {require('./../assets/images/icons/lego.svg')}
              scene               = 'lego'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "fan-icon"
              icon                = {require('./../assets/images/icons/fan.svg')}
              scene               = 'fan'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
            <HoverButton 
              id                  = "trophy-icon"
              icon                = {require('./../assets/images/icons/trophy.svg')}
              scene               = 'trophy'
              circle              = {circle}
              onHoverComplete     = {this._onHoverComplete}
              onHoverDetails      = {this.props.onHoverDetails}
            />
          </div>
        </section>
        <section className="room-bg" data-depth="0.1"></section>
        <section id="ambience-video" data-depth="0.1">
          <video
            id          = 'bg-video'
            ref         = {(bg) => { this.bgVideo = bg }}
            className   = 'window-day'
            loop        = {true}
            muted       = {true}
            autoPlay    = {true}
            playsInline = {true}
            controls    = {false}
            src         = { astronomy == 'day' ? require('./../assets/images/day.mp4') : astronomy == 'dusk' ? require('./../assets/images/dusk.mp4') : require('./../assets/images/night.mp4')}
          ></video>
        </section>
      </section>
    );
  }
}

export default DarkRoom;