import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 3000);
  };
  

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#lego-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.setState({ videoPlaying: true });
          this.setState({ audioPlaying: true });
        }, 500);

        setTimeout(() => {
          $('#lego-value').animateNumber({
            number: 90,
            easing: 'easeInOutExpo',
          }, 2250);
        }, 1000);

        let legoAnime = anime.timeline({})
        .add({
          targets : '#lego-scene .legend',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        .add({
          targets   : '#lego-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets   : '#lego-scene .description',
          translateX: [-590, 0],
          translateZ: 0,
          easing    : "linear",
          duration  : 2250,
          offset    : 1000,
        })
        // .add({
        //   targets   : '#lego-line',
        //   opacity   : [0,1],
        //   easing    : "easeOutExpo",
        //   duration  : 500,
        //   offset    : '+=200'
        // })
        .add({
          targets : '#lego-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#lego-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        $('#lego-value').html('0');
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "lego-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div id="lego-animate-block" className="scene-value">
            <div className="description">
              <h2 className="percentage digits"><span id="lego-value">0</span>%</h2>
              <h4 className="desc">of sexual predators are people</h4>
              <h4 className="desc">children know, love and trust.</h4>
            </div>
          </div>

          {/* <div id="lego-line">
            <img src={require('./../assets/images/line-lego.png')} alt=""/>
          </div> */}

          <div className="legend">
            <div className="legend-card">
              <h6 className="legend-title">Stranger</h6>
              <img src={require('./../assets/images/legend/lego-pale.png')} alt="" className="legend-image"/>
            </div>

            <div className="legend-card">
              <h6 className="legend-title">Known Acquaintances</h6>
              <img src={require('./../assets/images/legend/lego-rainbow.png')} alt="" className="legend-image"/>
            </div>
          </div>

          <div className="cta-buttons button-list vertical-buttons">
            <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
              <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
            </a>
            <a href="#" className="btn btn-default btn-sm btn-share">
              <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
              <HoverShare scene='lego'/>
            </a>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;