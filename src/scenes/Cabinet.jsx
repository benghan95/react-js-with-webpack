import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#cabinet-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/(\b[^\s]+|-+)/g, "<span class='letter'>$&</span>"));
        });

        this.setState({ audioPlaying: true });

        setTimeout(() => {
          this.setState({ videoPlaying: true });
        }, 2000);

        setTimeout(() => {
          $('#cabinet-value').animateNumber({
            number: 84,
            easing: 'easeInOutExpo',
          }, 1800);
        }, 2000);

        let bandsAnime = anime.timeline({})
        .add({
          targets : '#cabinet-scene .legend',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        .add({
          targets   : '#cabinet-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '+=2000',
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets : '#cabinet-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#cabinet-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        $('#cabinet-value').html('0');
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "cabinet-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div className="col-md-6 d-flex justify-content-end align-items-center">
            <div className="description">
              <div className="scene-value">
                <h2 className="percentage"><span id="cabinet-value">0</span>%</h2>
                <h4 className="desc">of rape cases involve</h4>
                <h4 className="desc">victims 10 - 18 years old.</h4>
              </div>

              <div className="legend">
                <div className="legend-card">
                  <img src={require('./../assets/images/legend/cabinet-color.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">10 - 18 years old</h6>
                </div>

                <div className="legend-card">
                  <img src={require('./../assets/images/legend/cabinet-plain.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">Above 18 years old</h6>
                </div>
              </div>

              <div className="cta-buttons button-list">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm btn-share">
                  <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                  <HoverShare scene='cabinet'/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;