import React, { Component } from 'react'
import anime from 'animejs'
import Menu from './components/Menu'
import Preloader from './components/Preloader'
import DarkRoom from './components/DarkRoom'
import Details from './components/Details'
import ProgressBar from 'progressbar.js'
import Room from './components/Room'
import Header from './components/Header'
import Footer from './components/Footer'
import '../../bootstrap/fonts/vidaloka.scss'
import { styles } from './styles.scss'
import { linearicons } from '../../../node_modules/linearicons/dist/web-font/style.css'

let circle    = null;
let moveForce = 10;    // max popup movement in pixels
let itemForce = 2;     // max popup movement in pixels

class Home extends Component {
	constructor (props){
		super(props);
		this.state = {
			width       : 0,
			height      : 0,
			focused     : false,
			focusedScene: null,
			// focused       : true,
			// focusedScene  : 'trophy',
			didMount: false,
			circle  : null,
			// passPreloader : true,
			// preloaderEnded: true,
			passPreloader : false,
			preloaderEnded: false,
			menuOpen      : false,
			onButton      : false,
			cursorSize    : '35px',
			zoomDark      : false,
			// zoomDark: true,
		}
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}

	componentDidMount(){

		circle = new ProgressBar.Circle('#cursor', {
			strokeWidth: 10,
			easing     : 'easeInOut',
			duration   : 1400,
			color      : '#ed1b24',
			trailColor : '#fff',
			trailWidth : 5,
			svgStyle   : {
					width: '100%',
			}
		});

		this.setState({
			circle  : circle,
			didMount: true,
		})

		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
			window.removeEventListener('resize', this.updateWindowDimensions);
	}

	updateWindowDimensions() {
			this.setState({
					width : window.innerWidth,
					height: window.innerHeight
			});
	}

	onPassPreloader = (passPreloader) => {
		this.setState({ passPreloader: passPreloader });
	}

	preloaderCallback = (preloaderEnded) => {
		this.setState({ preloaderEnded: preloaderEnded });
	}

	onHoverDetails = (scene, status) => {
		if (status){
			this.setState({
				focusedScene: scene
			})
		}
	}

	onHoverIconCallback = (size) => {
		// const cursorSize = size + 'px';
		// this.setState(prevState => ({
		// 	cursorSize : cursorSize,
		// 	cursorStyle: {
		// 		opacity: prevState.cursorStyle.opacity,
		// 		left   : prevState.cursorStyle.left,
		// 		top    : prevState.cursorStyle.top,
		// 		width  : cursorSize,
		// 	}
		// }));
	}

	onReturnRoom = (state) => {
		if (state){
			let returnRoomAnime = anime.timeline({})
			.add({
					targets : '.room-details',
					scale   : '1',
					easing  : "easeInCirc",
					duration: 1000,
			})
			.add({
					targets : '#return-room',
					opacity : [1, 0],
					easing  : "easeInExpo",
					duration: 500,
					offset  : '-=250'
			})
			.add({
					targets : '.room-details',
					opacity : [1, 0],
					easing  : "easeInCirc",
					duration: 500,
					offset  : '-=500'
			})
			.add({
					targets : '.hover-button',
					scale   : '1',
					easing  : "easeInCirc",
					duration: 0,
					offset  : '-=500'
			})
			;

			returnRoomAnime.complete = () => {
				this.setState({
					focused: false,
				});
				this.onZoom(false);
				this.onHoverIconCallback(35);
				this.state.circle.animate(0, { duration: 500 });
				anime.timeline({})
				.add({
						targets : '#menu',
						opacity : [0, 1],
						easing  : "easeInExpo",
						duration: 500,
				})
				.add({
						targets : '.room-details',
						scale   : '1.1',
						opacity : 1,
						easing  : "easeInExpo",
						duration: 0,
				})
			}
		}
	}

	onToggleMenuBtn = () => {
		this.setState((prevState) => { return {
				menuOpen: !prevState.menuOpen
		}});
	}

	onButtonCallback = (onButton) => {
		this.setState({ onButton: onButton });
	}

	_onMouseMove = (e) => {
		e.persist();
		this.setState(prevState => ({
			cursorStyle: {
				opacity: 1,
				left   : e.pageX + 'px',
				top    : e.pageY + 'px',
				width  : prevState.cursorSize,
			},
		}));

		if (this.state.zoomDark){
			var docX = this.state.width;
			var docY = this.state.height;

			var moveX = 50 + (e.pageX - docX/2) / (docX/2) * moveForce;
			var moveY = 50 + (e.pageY - docY/2) / (docY/2) * moveForce;

			// console.log("PageX: " + moveX);
			// console.log("PageY: " + moveY);

			var itemX = 50 + (e.pageX - docX/2) / (docX/2) * -itemForce;
			var itemY = 50 + (e.pageY - docY/2) / (docY/2) * -itemForce;

			this.setState({
					roomStyle: {
							backgroundPosition: moveX + '% ' + moveY + '%',
					},
					roomItemsStyle: {
							left: itemX + '%',
							top : itemY + '%',
					},
					quoteStyle: {
							// left: (6 +  moveX) + '%',
							// top : (69 + moveY) + '%',
					},
			});
		}
	}

	onHoverEnd = (state) => {
		if(state){
			this.setState({
				zoomDark: true
			})
			this.state.circle.animate(0, { duration: 500 });
		}
	}

	onZoom = (zoomed) => {
		if(zoomed){
			this.setState({
				focused: true,
			})
		}
		else{
			this.setState({
				focused: false,
			})
		}
	}

	render() {
		return (
			<main
				id          = "page-home"
				onMouseMove = {e => this._onMouseMove(e)}>
				<div id="cursor" style={this.state.cursorStyle}></div>
				{/* <section id="init-loader" className={(this.state.didMount ? 'd-none' : '')}></section> */}
				<Header
					className           = {(this.state.preloaderEnded) ? '' : 'd-none' }
					focused             = {this.state.focused}
					focusedScene             = {this.state.focusedScene}
					ticketId            = {1}
					menuOpen            = {this.state.menuOpen}
					onToggleMenuBtn     = {this.onToggleMenuBtn}
					onHoverIconCallback = {this.onHoverIconCallback}
					onReturnRoom        = {this.onReturnRoom}
					onZoom              = {this.onZoom}
					circle              = {this.state.circle} />
				<Menu menuOpen = {this.state.menuOpen}/>
				<Footer className={(this.state.preloaderEnded) ? '' : 'd-none' }/>
				{this.state.preloaderEnded ? null :
					<Preloader 
						mounted           = {this.state.didMount}
						onPassPreloader   = {this.onPassPreloader}
						preloaderCallback = {this.preloaderCallback}
					/>
				}
				{this.state.passPreloader && !this.state.zoomDark ?
					<Room
						circle           = {this.state.circle}
						onButtonCallback = {this.onButtonCallback}
						onHoverEnd       = {this.onHoverEnd}
						roomClass        = {this.state.roomClass}
					/> : null
				}
				{this.state.passPreloader ?
					<DarkRoom
						roomStyle           = {this.state.roomStyle}
						roomItemsStyle      = {this.state.roomItemsStyle}
						focused             = {this.state.focused}
						reached             = {this.state.zoomDark}
						circle              = {this.state.circle}
						onHoverDetails      = {this.onHoverDetails}
						onHoverIconCallback = {this.onHoverIconCallback}
						onZoom              = {this.onZoom} /> : null
				}
				{this.state.passPreloader ?
					<Details
						scene        = {this.state.focusedScene}
						focused      = {this.state.focused}
						detailsClass = {this.state.detailsClass}
					/> : null
				}
			</main>
		);
	}
}

export default Home;