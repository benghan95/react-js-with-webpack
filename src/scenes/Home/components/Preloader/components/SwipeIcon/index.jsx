import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import { styles } from './styles.scss'
import anime from 'animejs'

let iconMax = 205;
let lineMax = 150;
let swiped  = 0;

class SwipeIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      iconStyle: { left: '105%' },
      lineStyle: { width: '50%' },
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.swiped != this.props.swiped){
      swiped = this.props.swiped;

      let iconLeft  = '';
      let lineWidth = '';
      if (swiped > 1){
        iconLeft  = iconMax + '%';
        lineWidth = lineMax + '%';
      }
      else{
        iconLeft  = (105 + (iconMax - 105) * swiped) + '%';
        lineWidth = (50 + (lineMax - 50) * swiped) + '%';
      }
      this.setState({
        iconStyle: { left: iconLeft },
        lineStyle: { width: lineWidth },
      });
    }
  }

  render() {
    return (
      <div className="swipe-icon">
        <div className="swipe-wrapper">
          <div className="swipe-line" style={this.state.lineStyle}></div>
          <div className="arrow-icon" style={this.state.iconStyle}>
            <FontAwesome name='angle-right' />
          </div>
        </div>
      </div>
    );
  }
}

export default SwipeIcon;