import React, { Component } from 'react'
import anime from 'animejs'
import ProgressBar from 'progressbar.js'
import Quotes from './components/Quotes'
import SwipeIcon from './components/SwipeIcon'
import { styles } from './styles.scss'
import { TweenMax, TextPlugin, TweenLite, TimelineMax } from "gsap";

let dragDurationNeeded = 50;

let progressBar;

let quoteAnime;

class Preloader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mounted       : false,
      loaded        : false,
      progress      : 0,
      onHold        : false,
      startX        : 0,
      startY        : 0,
      swiped        : 0,
      canRelease    : false,
      quoteCount    : 1,
      swipeClass    : 'swipe fade',
      blurrish      : '0',
      preloaderEnded: false,
    };
  }

  /**
  |--------------------------------------------------
  | Component functions
  |--------------------------------------------------
  */
  componentDidMount(){
    // this.onStart(this.props.mounted);
  }

  componentWillUnmount(){
    // this.onStart(this.props.mounted);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.mounted !== this.props.mounted) {
      this.setState({
        mounted: true,
      })
      if (nextProps.mounted && !this.state.mounted){
        $('#statistics h3').each(function(){
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"))
          // $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        progressBar = new ProgressBar.Line('#progress', {
          duration   : 500,
          strokeWidth: 3,
          easing     : 'easeInOut',
          color      : '#fc0e29',
          trailColor : 'transparent',
          trailWidth : 0,
          svgStyle   : {
            display: 'block',
            width  : '100%'
          },
          from: { color: '#fff000' },
          to  : { color: '#ed1b24' },
          step: function(state, circle, attachment) {
              circle.path.setAttribute('stroke', state.color);
          },
        });

        setTimeout(() => {
          this.initQuoteAnime();
          this.loopQuoteAnime();
        }, 1000);

        // setTimeout(() => { this.progressInc() }, 1000)
        // setTimeout(() => { this.progressInc() }, 3000)
        // setTimeout(() => { this.progressInc() }, 5000)
        // setTimeout(() => { this.progressInc() }, 7000)
        // setTimeout(() => { this.progressDone() }, 10000)

        setTimeout(() => { this.progressInc() }, 1000)
        setTimeout(() => { this.progressInc() }, 2000)
        setTimeout(() => { this.progressInc() }, 3000)
        setTimeout(() => { this.progressInc() }, 4000)
        setTimeout(() => { this.progressDone() }, 5000)
      }
    }
  }


  /**
  |--------------------------------------------------
  | ProgressBar functions
  |--------------------------------------------------
  */
  progressStart = () => {
    this.setState({
      progress: 0,
    })
  }

  progressInc = () => {
    let max         = 0.9 * 100
    let min         = this.state.progress * 100
    let newProgress = Math.floor(Math.random() * (max - min) + min) / 100

    this.setState({
      progress: newProgress,
    });

    progressBar.animate(this.state.progress, {
      duration: 800
    });
  }

  progressDone = () => {
    progressBar.animate(1, {
      duration: 800
    }, () => {
      this.setState({
        loaded    : true,
        swipeClass: 'swipe fade in',
      });
    });
  }

  /**
  |--------------------------------------------------
  | Quote Animation functions
  |--------------------------------------------------
  */
  loopQuoteAnime = () => {
    quoteAnime.complete = ()  => {
      let max     = 2;
      let current = this.state.quoteCount;

      if (current < max){
        this.setState( (prevState) => ({
          quoteCount: prevState.quoteCount + 1
        }));
      }
      else{
        this.setState({
          quoteCount: 1
        });
      }

      $('#statistics h3').each(function(){
        $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        // $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
      });

      this.initQuoteAnime();
      this.loopQuoteAnime();
    }
  }

  initQuoteAnime = () => {
    quoteAnime = anime.timeline()
    .add({
      targets   : '#statistics h3 .letter',
      translateX: [40,0],
      translateZ: 0,
      opacity   : [0,1],
      easing    : "easeOutExpo",
      duration  : 3000,
      delay     : function(el, i) {
        return 500 + 30 * i;
      }
    })
    .add({
      targets : '#statistics h5',
      opacity : [0,1],
      easing  : "easeInOutQuad",
      duration: 1500,
      offset  : '-=2000',
    })
    .add({
      targets   : '#statistics h3 .letter',
      translateX: [0,-30],
      opacity   : [1,0],
      easing    : "easeInExpo",
      duration  : 1100,
      offset    : '+=2000',
      delay     : function(el, i) {
        return 100 + 30 * i;
      }
    })
    .add({
      targets : '#statistics h5',
      opacity : 0,
      duration: 1000,
      easing  : "easeOutExpo",
      offset  : '-=1000'
    });
    // .add({
    //   targets : '#statistics h3 .letter',
    //   opacity : [0,1],
    //   easing  : "easeInOutQuad",
    //   duration: 500,
    //   delay   : function(el, i) {
    //     return 300 * (i+1)
    //   },
    // })
    // .add({
    //   targets : '#statistics h5',
    //   opacity : [0,1],
    //   duration: 1500,
    //   easing  : "easeInOutQuad",
    // })
    // .add({
    //   targets : '#statistics h3 .letter',
    //   opacity : 0,
    //   duration: 1000,
    //   easing  : "easeOutExpo",
    //   delay   : 3000
    // })
    // .add({
    //   targets : '#statistics h5',
    //   opacity : 0,
    //   duration: 1000,
    //   easing  : "easeOutExpo",
    //   offset  : '-=1000'
    // });
  }

  /**
  |--------------------------------------------------
  | Swipe functions
  |--------------------------------------------------
  */
  onHold = (e) => {
    if(this.state.loaded){
      if(this.state.canRelease){
        this.setState({
          onHold  : true,
          startX  : e.pageX,
          startY  : e.pageY,
          blurrish: 5,

          swiped: 1,
        });
      }
      else{
        quoteAnime.pause();
        this.setState({
          onHold: true,
          startX: e.pageX,
          startY: e.pageY,

          blurrish: 1,

          swiped: 0,
        });
      }
    }
  }

  onSwipe = (e) => {
    if (this.state.onHold && this.state.loaded){
      let difference = e.pageX - this.state.startX
      if (difference > 0){
        let blurrish = difference / dragDurationNeeded * 3;
        this.setState({
          swiped: difference / dragDurationNeeded,

          blurrish: 1 + ((blurrish > 5) ? 5   : blurrish),
        });

        if (difference > dragDurationNeeded){
          this.setState({
            canRelease: true,

            blurrish  : 3,
          })
        }
      }
      else{
        this.setState({
          blurrish: 0,
        });
      }
    }
  }
  onRelease = () => {
    if(this.state.loaded){
      if (this.state.canRelease){
        this.setState({
          onHold: false,
          swiped: 1,

          blurrish: 3,
        });
        if (!this.state.preloaderEnded){
          this.props.onPassPreloader(true);
          // let hidePreloaderTl = new TimelineMax();
          // hidePreloaderTl
          //   .set($('#preloader .swipe'), { opacity: 0 }, 1)
          //   .set($('#preloader'), { scale: '1.1', x: '0'}, 1.5)
          //   .set($('#preloader'), {webkitFilter:"blur(1px)", x: '20px'}, 2)
          //   .set($('#preloader'), {webkitFilter:"blur(2px)", x: '-20px'}, 2.15)
          //   .set($('#preloader'), {webkitFilter:"blur(3px)", x: '0', y: '20px'}, 2.3)
          //   .set($('#preloader'), {webkitFilter:"blur(4px)", x: '0', y:'-20px'}, 2.45)
          //   .set($('#preloader'), {webkitFilter:"blur(5px)", x: '0', y: '0'}, 2.6)
          //   .set($('#preloader'), { scale: '1.1', x: '0'}, 3)
          //   .set($('#preloader'), { scale: '1.2'}, 3.15)
          //   .set($('#preloader'), { scale: '1.3'}, 3.3)
          //   .set($('#preloader'), { scale: '1.4'}, 3.45)
          //   .set($('#preloader'), { scale: '1.5'}, 3.6)
          //   .set($('#preloader'), { scale: '1.6'}, 3.75)
          //   .set($('#preloader'), { opacity: 0, scale: '1.5'}, 3.6)

          let preloaderEndAnime = anime.timeline({})
          .add({
            targets : '#preloader .swipe',
            opacity : [1, 0],
            easing  : "easeInExpo",
            duration: 1000,
          })
          .add({
            targets : '#preloader',
            scale   : '1.3',
            easing  : "easeInQuart",
            duration: 1500,
            offset  : '+=1000'
          })
          .add({
            targets : '#preloader',
            opacity : [1, 0],
            easing  : "easeOutCubic",
            duration: 500,
            offset  : '-=250',
          });

          // hidePreloaderTl.eventCallback("onComplete", () => {
          //   this.setState({
          //     preloaderEnded: true,
          //   });
          //   this.props.preloaderCallback(true);
          // });

          preloaderEndAnime.complete = () => {
            this.setState({
              preloaderEnded: true,
            });
            this.props.preloaderCallback(true);
          }

          anime.timeline({}).add({
            targets   : '#preloader',
            visibility: 'hidden',
            easing    : "easeInOutQuad",
            duration  : 0,
          })
        }
      }
      else{
        quoteAnime.play();
        this.setState({
          canRelease: false,
          onHold    : false,
          swiped    : 0,
          blurrish  : 0,
        })
      }
    }
  }

  /**
   |--------------------------------------------------
   | Render function
   |--------------------------------------------------
   */
  render() {
		return (
      <section
        id           = "preloader"
        className    = "d-flex"
        style        = {(this.state.preloaderEnded) ? {visibility: 'hidden'} : {} }
        onTouchStart = {e => this.onHold(e.changedTouches[0])}
        onTouchMove  = {e => this.onSwipe(e.changedTouches[0])}
        onTouchEnd   = {() => this.onRelease()}
        onMouseDown  = {e => this.onHold(e)}
        onMouseMove  = {e => this.onSwipe(e)}
        onMouseUp    = {() => this.onRelease()}
        onMouseLeave = {() => this.onRelease()}>
        <div className="container-fluid wrapper" style={{filter: 'blur(' + this.state.blurrish + 'px)'}}>
          <div className="row h-100 w-100 m-0" style={{display: (this.state.preloaderEnded) ? 'none' : '' }}>
            <div className="col d-flex flex-column text-center p-0">
              <section id="music-notice" className="d-flex align-items-end justify-content-center">
                <div>
                  <i className="fa fa-headphones icon-headphones"></i>
                  <h6>Best Experienced with Sound</h6>
                </div>
              </section>
              <section id="statistics">
                <Quotes quote={this.state.quoteCount}/>
              </section>
            </div>
          </div>
          <div id="progress" className={(this.state.loaded) ? 'fade-out' : ''}></div>
        </div>

        <div className={this.state.swipeClass} >
          <SwipeIcon swiped={this.state.swiped} />
          <p style={{filter: 'blur(' + this.state.blurrish / 5  + 'px)'}}>Swipe to unlock</p>
        </div>
      </section>
		);
	}
}

export default Preloader;