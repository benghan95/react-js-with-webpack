import React, { Component } from 'react';
import { styles } from './styles.scss';
import anime from 'animejs'
import './../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import Bed from './components/Bed';
import Board from './components/Board';
import Bands from './components/Bands';
import Cabinet from './components/Cabinet';
import Shelves from './components/Shelves';
import Clock from './components/Clock';
import Lego from './components/Lego';
import Fan from './components/Fan';
import Trophy from './components/Trophy';

class RoomDetails extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  };

  render() {
    switch (this.props.scene){
      case 'bed': 
        return (
          <Bed 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'board': 
        return (
          <Board 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'bands': 
        return (
          <Bands 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'cabinet': 
        return (
          <Cabinet 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'shelves': 
        return (
          <Shelves 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'clock': 
        return (
          <Clock 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'lego': 
        return (
          <Lego 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'fan': 
        return (
          <Fan 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      case 'trophy': 
        return (
          <Trophy 
            focused      = {this.props.focused}
            detailsClass = {this.props.detailsClass}
          />
        );
      default: 
        return (
          null
        );
    };
    // return (
    //   <section
    //     className = {`room-details ${this.props.focused ? 'focused' : ''}`}>
    //     <div className="bg-overlay"></div>
    //     <video
    //       ref = "video"
    //       src = {require('./../assets/images/stop-motion/06_Bed.mp4')}
    //     ></video>
    //     <div className="row">
    //       <div className="col-md-7 text-center left">
    //         <div className="left-info">
    //           <div className="text-center">
    //             <h2><span id="bed-counter">0</span><span className="small">%</span></h2>
    //             <h5 className={this.state.done ? '' : 'd-none'}>of these cases involved rape.</h5>
    //           </div>
    //         </div>
    //       </div>
    //       <div className="col-md-5 right">
    //         <div className="right-info">
    //           <div className="stats">
    //             <h3><span id="side-counter">22,134</span></h3>
    //             <h4>Malaysian children were sexually abused<br />between 2010 and 2017.</h4>
    //             <div className="button-list">
    //               <a href="#" className="btn btn-default">Learn How to Stop Them</a>
    //               <a href="#" className="btn btn-default btn-sm">
    //                 <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
    //               </a>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //   </section>
    // );
  }
}

export default RoomDetails;