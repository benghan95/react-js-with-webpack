import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import { TweenMax, TextPlugin, TweenLite, TimelineMax } from "gsap";

let percentage = 100;

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done     : false,
      completed: false,
    }
  }

  changeValue = (newtext) => {
    $("#fan-value").text(newtext);
  }

  videoCompleted = () => {
    this.setState({
      completed: true,
    })
  }

  componentDidMount = () => {
  };


  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        this.refs.endVideo.play();
        this.refs.endVideo.loop = true;
        this.refs.video.play();
        document.getElementById('initial-video').addEventListener('ended', this.videoCompleted, false);

        $('#fan-value').text('0');
        $('#fan-scene .scene-value').css("transform", "translateX(-87%) scale(0.5)");
        $('#fan-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          // $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          $('#fan-value').animateNumber({
            number: 100,
            easing: 'easeInQuad',
          }, 750);
        }, 1000);

        setTimeout(() => {
          $('#fan-value').prop('number', 100)
          .animateNumber
          ({
            number: 200,
            easing: 'easeInQuad',
          }, 750);
        }, 3000);

        setTimeout(() => {
          $('#fan-value').prop('number', 200)
          .animateNumber({
            number: 300,
            easing: 'easeInQuad',
          }, 750);
        }, 5000);

        let fanValue = anime.timeline({})
        .add({
          targets   : '#fan-scene .scene-value',
          translateX: '-70%',
          scale     : 0.65,
          easing    : "easeInQuad",
          duration  : 1000,
          offset    : 1000
        })
        .add({
          targets   : '#fan-scene .scene-value',
          translateX: '-33%',
          scale     : 0.8,
          easing    : "easeInQuad",
          duration  : 1000,
          offset    : 3000
        })
        .add({
          targets   : '#fan-scene .scene-value',
          translateX: '0%',
          scale     : 1,
          easing    : "easeInQuad",
          duration  : 1000,
          offset    : 5000
        })

        let fanAnime = anime.timeline({})
        .add({
          targets : '#fan-scene .line-1',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
          offset  : 1000
        })
        .add({
          targets: '#fan-scene .line-2',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#fan-scene .line-3',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#fan-scene .line-4',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#dotted-line .point-1',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 10,
        })
        .add({
          targets : '#fan-scene .line-5',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
          offset  : '+=1000'
        })
        .add({
          targets: '#fan-scene .line-6',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#fan-scene .line-7',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#fan-scene .line-8',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#dotted-line .point-2',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 10,
        })
        .add({
          targets : '#fan-scene .line-9',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
          offset  : '+=1000'
        })
        .add({
          targets: '#fan-scene .line-10',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#fan-scene .line-11',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#fan-scene .line-12',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 250,
        })
        .add({
          targets: '#dotted-line .point-3',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 1000,
        })
        .add({
          targets   : '#fan-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 3000,
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets : '#fan-scene .cta-buttons',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        ;


        // let fanTl = new TimelineMax();
        // fanTl
        //   .set($('#fan-scene .scene-value'), {transformOrigin:"bottom left"}, 0)
        //   .set($('#fan-scene .scene-value'), {x: '-87%', scale: '0.5'}, 0)
        //   .call(this.changeValue, ["0"])
        //   .set($('#fan-scene .scene-value'), {x: '-80%', scale: '0.525'}, 1)
        //   .set($('#dotted-line .line-1'), { opacity: 1 }, 0)
        //   .call(this.changeValue, ["23"])
        //   .set($('#fan-scene .scene-value'), {x: '-80%', scale: '0.55'}, 1.25)
        //   .set($('#dotted-line .line-2'), { opacity: 1 }, 1)
        //   .call(this.changeValue, ["58"])
        //   .set($('#fan-scene .scene-value'), {x: '-80%', scale: '0.575'}, 1.5)
        //   .set($('#dotted-line .line-3'), { opacity: 1 }, 1.25)
        //   .call(this.changeValue, ["79"])
        //   .set($('#fan-scene .scene-value'), {x: '-70%', scale: '0.65'}, 1.75)
        //   .set($('#dotted-line .line-4'), { opacity: 1 }, 1.5)
        //   .set($('#dotted-line .point-1'), { opacity: 1 }, 1.75)
        //   .call(this.changeValue, ["100"])
        //   .set($('#fan-scene .scene-value'), {x: '-58.8%', scale: '0.725'}, 2.75)
        //   .set($('#dotted-line .line-5'), { opacity: 1 }, 2.75)
        //   .call(this.changeValue, ["126"])
        //   .set($('#fan-scene .scene-value'), {x: '-50%', scale: '0.750'}, 3)
        //   .set($('#dotted-line .line-6'), { opacity: 1 }, 3)
        //   .call(this.changeValue, ["148"])
        //   .set($('#fan-scene .scene-value'), {x: '-41.5%', scale: '0.75'}, 3.25)
        //   .set($('#dotted-line .line-7'), { opacity: 1 }, 3.25)
        //   .call(this.changeValue, ["183"])
        //   .set($('#fan-scene .scene-value'), {x: '-33%', scale: '0.8'}, 3.5)
        //   .set($('#dotted-line .line-8'), { opacity: 1 }, 3.5)
        //   .set($('#dotted-line .point-2'), { opacity: 1 }, 3.5)
        //   .call(this.changeValue, ["200"])
        //   .set($('#fan-scene .scene-value'), {x: '-24.75%', scale: '0.825'}, 4.5)
        //   .set($('#dotted-line .line-9'), { opacity: 1 }, 4.5)
        //   .call(this.changeValue, ["219"])
        //   .set($('#fan-scene .scene-value'), {x: '-16.5%', scale: '0.850'}, 4.75)
        //   .set($('#dotted-line .line-10'), { opacity: 1 }, 4.75)
        //   .call(this.changeValue, ["268"])
        //   .set($('#fan-scene .scene-value'), {x: '-8.25%', scale: '0.875'}, 5)
        //   .set($('#dotted-line .line-11'), { opacity: 1 }, 5)
        //   .call(this.changeValue, ["293"])
        //   .set($('#fan-scene .scene-value'), {x: '0%', scale: '1'}, 5.25)
        //   .set($('#dotted-line .line-12'), { opacity: 1 }, 5.25)
        //   .set($('#dotted-line .point-3'), { opacity: 1 }, 5.25)
        //   .call(this.changeValue, ["300"])
      }
      else{
        this.refs.video.load();
        this.refs.endVideo.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "fan-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/fan.jpg')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "endVideo"
          id  = "end-video"
          src = { require('./../assets/images/stop-motion/fan-end.mp4') }
        ></video>
        <video
          ref = "video"
          id  = "initial-video"
          className= {this.state.completed ? 'd-none': null}
          src = { require('./../assets/images/stop-motion/fan.mp4')}
        ></video>
        <svg id="dotted-line" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.08 5.61">
          <g className="line line-1">
            <path d="M13.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M10.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M8.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M5.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M3.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,1,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M.88,5.56H.38a.38.38,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-2">
            <path d="M28.37,5.61h-.5a.37.37,0,1,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M25.87,5.59h-.5a.37.37,0,1,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M23.37,5.57h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M20.87,5.55h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M18.37,5.54h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,1,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M15.87,5.52h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,1,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-3">
            <path d="M43.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M40.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M38.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M35.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M33.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M30.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-4">
            <path d="M58.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M55.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M53.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M50.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M48.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M45.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-5">
            <path d="M73.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M70.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M68.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M65.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M63.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M60.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-6">
            <path d="M88.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M85.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M83.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M80.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M78.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M75.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-7">
            <path d="M103.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M100.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M98.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M95.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M93.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M90.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-8">
            <path d="M115.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M113.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M110.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M108.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M105.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-9">
            <path d="M130.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M128.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M125.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M123.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M120.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M118.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-10">
            <path d="M145.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M143.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M140.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M138.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M135.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M133.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-11">
            <path d="M160.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M158.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M155.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M153.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M150.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M148.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>
          <g className="line line-12">
            <path d="M173.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M170.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M168.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M165.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
            <path d="M163.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" style={{ fill: '#fff' }} />
          </g>

          <path className="point point-1" d="M58.11,5.09a.38.38,0,0,1-.37-.37v-.5a.38.38,0,0,1,.75,0v.5A.38.38,0,0,1,58.11,5.09Zm0-2.5a.38.38,0,0,1-.37-.37v-.5a.38.38,0,0,1,.75,0v.5A.38.38,0,0,1,58.11,2.59Z" style={{ fill: '#fff' }} />
          <path className="point point-2" d="M115.61,5.09a.38.38,0,0,1-.37-.37v-.5a.38.38,0,1,1,.75,0v.5A.38.38,0,0,1,115.61,5.09Zm0-2.5a.38.38,0,0,1-.37-.37v-.5a.38.38,0,1,1,.75,0v.5A.38.38,0,0,1,115.61,2.59Z" style={{ fill: '#fff' }} />
          <g className="point point-3">
            <circle cx="173.14" cy="0.94" r="0.94" style={{ fill: '#fff' }} />
            <path d="M173.14,3.92a.38.38,0,0,1-.37-.37V3a.38.38,0,0,1,.75,0v.5A.38.38,0,0,1,173.14,3.92Zm0-2.5a.38.38,0,0,1-.37-.37V.55a.38.38,0,1,1,.75,0V1A.38.38,0,0,1,173.14,1.42Z" style={{ fill: '#fff' }} />
          </g>
        </svg>

        <div className="row content">
          <div className="col-md-6 offset-md-6 d-flex align-items-center justify-content-center">
            <div className="description">
              <h4 className="desc">Within 5 years in Malaysia, </h4>
              <h4 className="desc">Internet-initiated rape</h4>
              <h4 className="desc">cases increased</h4>
              <h2 className="scene-value digits"><span id="fan-value">300</span>%</h2>
              <div className="cta-buttons button-list">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm">
                  <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;