import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done     : false,
      completed: false,
    }
  }

  componentDidMount = () => {
    // this.setState({
    //   completed: true
    // })
    // setTimeout(() => {
    // }, 1500);
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused && !this.state.completed){
        $('#bed-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');

        setTimeout(() => {
          $('#side-counter').animateNumber({
            number    : 22134,
            numberStep: comma_separator_number_step,
            easing    : 'easeInOutExpo',
          }, 2000);
        }, 2000);

        setTimeout(() => {
          $('#bed-counter').animateNumber({
            number    : 13272,
            numberStep: comma_separator_number_step,
            easing    : 'easeInOutExpo',
          }, 2000);

          this.refs.video.play();
        }, 7000);

        setTimeout(() => {
          this.setState({
            done: true,
          })
        }, 10000);

        let bedAnime = anime.timeline({})
        .add({
          targets   : '#bed-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : 4000,
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#bed-counter',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : 6000,
        })
        .add({
          targets   : '#bed-scene .legend',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : 12000,
        })
        .add({
          targets   : '#bed-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : 14000,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : 15000,
        })
        ;
      }
      else{
        this.refs.video.load();
        $('#side-counter').html(0);
        $('#bed-counter').html(0);
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "bed-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/bed.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/bed.mp4')}
        ></video>
        <div className="row">
          <div className="col-md-7 text-center left">
            <div className="left-info">
              <div className="text-center">
                <h2 className="digits"><span id="bed-counter">0</span></h2>
                <h5 className={this.state.done ? '' : 'hidden'}>of these cases involved rape.</h5>
              </div>
            </div>
          </div>
          {/* <div id="marker-line">
            <svg id="line-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.9 308.13">
              <polyline points="9.4 156.43 9.4 307.75 0.38 307.75" className="dashed-line"/>
              <polyline points="9.4 154.4 9.4 0.38 0.38 0.38" className="dashed-line"/>
              <g>
                <line x1="36.75" y1="155.43" x2="10.2" y2="155.43" className="dashed-line"/>
                <circle cx="38.97" cy="155.43" r="0.94" style={{fill: '#fff'}}/>
              </g>
            </svg>
          </div> */}
          <div className="col-md-5 right d-flex justify-content-center align-items-center">
            <div className="right-info">
              <div className="stats">
                <h3><span id="side-counter" className="digits">0</span></h3>
                <h4 className="desc">Malaysian children were sexually abused</h4>
                <h4 className="desc">between 2010 and 2017.</h4>
                <div className="legend">
                  <div className="legend-card">
                    <img src={require('./../assets/images/legend/bed-white.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Total Sexual Abuse Cases</h6>
                  </div>

                  <div className="legend-card">
                    <img src={require('./../assets/images/legend/bed-red.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Rape</h6>
                  </div>
                </div>
                <div className="cta-buttons button-list vertical-buttons">
                  <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                    <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
                  </a>
                  <a href="#" className="btn btn-default btn-sm">
                    <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;