import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#board-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.refs.video.play();
        }, 500);

        let boardAnime = anime.timeline({})
        .add({
          targets : '#wechat-percentage',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
          offset  : '+=1000'
        })
        .add({
          targets : '#messenger-percentage',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 750,
          offset  : '-=250'
        })
        .add({
          targets   : '#beetalk-percentage',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
        })
        .add({
          targets   : '#whatsapp-percentage',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
        })
        .add({
          targets : '#board-scene .special-title',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        .add({
          targets : '#board-scene .right-info h2',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
          offset  : '-=1000'
        })
        .add({
          targets   : '#board-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#board-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : '+=1000',
        })
        ;
      }
      else{
        this.refs.video.load();
      }
    }
  }

  render() {
    return (
      <section
        id        = "board-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/board.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/board.mp4')}
        ></video>
        <div className="row content">
          <div className="col-md-12 d-flex justify-content-center">
            <div className="description">
              <div className="scene-value">
                <div className="special-title">
                  <h1><span className="symbol">#</span>1</h1>
                </div>
                <div className="right-info">
                  <h2>modus operandi</h2>
                  <h4 className="desc">for sexual predators is via chat apps. WeChat is popular</h4>
                  <h4 className="desc">because it allows them to search for children nearby.</h4>
                </div>
              </div>

              <div className="cta-buttons button-list">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm">
                  <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
                </a>
              </div>
            </div>
          </div>

          <div className="percentage-value">
            <div id="wechat-percentage" className="percentage digits">75<span className="percent">%</span></div>
            <div id="messenger-percentage" className="percentage digits">17<span className="percent">%</span></div>
            <div id="beetalk-percentage" className="percentage digits">3<span className="percent">%</span></div>
            <div id="whatsapp-percentage" className="percentage digits">3<span className="percent">%</span></div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;