import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 3000);
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        setTimeout(() => {
          this.refs.video.play();

          $('#trophy-scene .rank.third h3').each(function(){
            $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          });
          $('#trophy-scene .desc').each(function(){
            $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          });

          let trophyAnime = anime.timeline({})
          .add({
            targets : '#trophy-scene .rank.third h2',
            scale   : [5,1],
            opacity : [0,1],
            easing  : "easeOutCirc",
            duration: 800,
            offset  : '+=2500',
            delay   : function(el, i) {
              return 800 * i;
            }
          })
          .add({
            targets   : '#trophy-scene .rank.third h3 .letter',
            translateX: [40,0],
            translateZ: 0,
            opacity   : [0,1],
            easing    : "easeOutExpo",
            duration  : 1200,
            delay     : function(el, i) {
              return 500 + 30 * i;
            }
          })
          .add({
              targets : '#trophy-scene .rank.first',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
          })
          .add({
              targets : '#trophy-scene .rank.second',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: '-=500'
          })
          .add({
              targets : '#trophy-scene .rank.forth',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: '-=500'
          })
          .add({
              targets : '#trophy-scene .rank.fifth',
              opacity : [0, 1],
              easing  : "easeInSine",
              duration: 500,
              offset: '-=500'
          })
          .add({
            targets   : '#trophy-scene .desc .letter',
            translateX: [40,0],
            translateZ: 0,
            opacity   : [0,1],
            easing    : "easeOutExpo",
            duration  : 1200,
            delay     : function(el, i) {
              return 500 + 30 * i;
            }
          })
          .add({
            targets : '#trophy-scene .cta-buttons',
            opacity : [0,1],
            easing  : "easeOutExpo",
            duration: 1000,
            offset  : '+=1000'
          })
          .add({
            targets : '#return-room',
            opacity : [0,1],
            easing  : "easeOutExpo",
            duration: 1500,
            offset  : '+=1000',
          })
        }, 500);
      }
      else{
        this.refs.video.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "trophy-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/trophy.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/trophy.mp4')}
        ></video>
        <div className="row content">
          <div className="col-md-12">
            <div className="rank first">
              <h2>1<sup className="ordinal">st</sup></h2>
            </div>
            <div className="rank second">
              <h2>2<sup className="ordinal">nd</sup></h2>
            </div>
            <div className="rank third focus">
              <h3>Malaysia ranks</h3>
              <h2>3<sup className="ordinal">rd</sup></h2>
              <h3>in ASEAN for possession and</h3>
              <h3>distribution of child pornography. </h3>
              <h3>There are no specific laws against this.</h3>
            </div>
            <div className="rank forth">
              <h2>4<sup className="ordinal">th</sup></h2>
            </div>
            <div className="rank fifth">
              <h2>5<sup className="ordinal">th</sup></h2>
            </div>

            <div className="cta-buttons button-list vertical-buttons">
              <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
              </a>
              <a href="#" className="btn btn-default btn-sm">
                <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;