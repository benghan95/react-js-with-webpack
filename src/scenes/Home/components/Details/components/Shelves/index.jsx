import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 2000);
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused) {
        $('#shelves-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.refs.video.play();
        }, 500);

        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        // var dot_separator_number_step = $.animateNumber.numberStepFactories.separator('.');

        setTimeout(() => {
          $('#side-counter').animateNumber({
            number    : 12987,
            numberStep: comma_separator_number_step,
            easing    : 'easeInOutExpo',
          }, 1000);
        }, 500);

        let shelvesAnime = anime.timeline({})
        .add({
          targets   : '#shelves-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#shelves-scene .left-info .percentage',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '+=150',
        })
        .add({
          targets   : '#shelves-scene .center-info h4',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=500',
        })
        .add({
          targets   : '#shelves-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1500',
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : '+=1500',
        })
        ;
      }
      else{
        this.refs.video.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "shelves-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/shelves.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/stacked-clothes.mp4')}
        ></video>
        <div className="row content">
          <div className="col-md-6 offset-md-6 d-flex align-items-center">
            <div className="description-block">
              <div className="description scene-value">
                <h3 id="side-counter" className="digits">0</h3>
                <div className="right-info">
                  <h4 className="desc">cases of child sexual abuse</h4>
                  <h4 className="desc">reported between 2012 and 2016.</h4>
                </div>
              </div>
            </div>

            <div className="center-info">
              <div className="left-info">
                <h2 className="percentage"><span id="shelves-value">1.08</span>%</h2>
                <h4>resulted in convictions.</h4>
              </div>

              <div className="cta-buttons button-list vertical-buttons">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm">
                  <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;