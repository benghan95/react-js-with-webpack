import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#cabinet-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.refs.video.play();
        }, 500);

        setTimeout(() => {
          $('#cabinet-value').animateNumber({
            number    : 84,
            easing    : 'easeInOutExpo',
          }, 2000);
        }, 500);

        let bandsAnime = anime.timeline({})
        .add({
          targets   : '#cabinet-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=2500',
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#cabinet-scene .legend',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
        })
        .add({
          targets   : '#cabinet-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : '+=1000',
        })
        ;
      }
      else{
        this.refs.video.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "cabinet-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/cupboard.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/wardrobe.mp4')}
        ></video>
        <div className="row content">
          <div className="col-md-6 d-flex justify-content-center align-items-center">
            <div className="description">
              <div className="scene-value">
                <h2 className="percentage"><span id="cabinet-value">0</span>%</h2>
                <h4 className="desc">of rape cases involve</h4>
                <h4 className="desc">victims 10 – 18 years old.</h4>
              </div>

              <div className="legend">
                <div className="legend-card">
                  <img src={require('./../assets/images/legend/cabinet-color.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">10 - 18 years old</h6>
                </div>

                <div className="legend-card">
                  <img src={require('./../assets/images/legend/cabinet-plain.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">Above 18 years old</h6>
                </div>
              </div>

              <div className="cta-buttons button-list">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm">
                  <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;