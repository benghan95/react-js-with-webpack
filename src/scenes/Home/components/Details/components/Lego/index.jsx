import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 3000);
  };
  

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#lego-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.refs.video.play();
        }, 500);

        setTimeout(() => {
          $('#lego-value').animateNumber({
            number    : 90,
            easing    : 'easeInOutExpo',
          }, 2000);
        }, 1250);

        let legoAnime = anime.timeline({})
        .add({
          targets   : '#lego-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=4000',
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#lego-scene .legend',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
        })
        .add({
          targets   : '#lego-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : '+=1000',
        })
        ;
      }
      else{
        this.refs.video.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "lego-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/lego.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/lego.mp4')}
        ></video>
        <div className="row content">
          <div id="lego-animate-block" className="scene-value">
            <h2 className="percentage digits"><span id="lego-value">0</span>%</h2>
            <h4 className="desc">of sexual predators are people</h4>
            <h4 className="desc">children know, love and trust.</h4>
          </div>

          <div className="legend">
            <div className="legend-card">
              <h6 className="legend-title">Stranger</h6>
              <img src={require('./../assets/images/legend/lego-pale.png')} alt="" className="legend-image"/>
            </div>

            <div className="legend-card">
              <h6 className="legend-title">Victims under 16 years old</h6>
              <img src={require('./../assets/images/legend/lego-rainbow.png')} alt="" className="legend-image"/>
            </div>
          </div>

          <div className="cta-buttons button-list vertical-buttons">
            <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
              <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
            </a>
            <a href="#" className="btn btn-default btn-sm">
              <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
            </a>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;