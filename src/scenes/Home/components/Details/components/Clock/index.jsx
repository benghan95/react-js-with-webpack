import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 2000)
  };


  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#clock-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.refs.video.play();
        }, 3000);

        var max_number = 15;

        var padding_zeros = '';
        for(var i = 0, l = max_number.toString().length; i < l; i++) {
          padding_zeros += '0';
        }

        var padded_now, numberStep = function(now, tween) {
          var target = $(tween.elem),
              rounded_now = Math.round(now);

          var rounded_now_string = rounded_now.toString()
          padded_now = padding_zeros + rounded_now_string;
          padded_now = padded_now.substring(rounded_now_string.length);

          target.prop('number', rounded_now).text(padded_now);
        };

        setTimeout(() => {
          $('#clock-counter').animateNumber({
            number    : max_number,
            numberStep: numberStep,
            easing    : 'easeInOutExpo',
          }, 4000);
        }, 3000);

        let clockAnime = anime.timeline({})
        .add({
          targets   : '#clock-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#clock-scene .timer',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
        })
        .add({
          targets   : '#clock-scene .legend',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=4000',
        })
        .add({
          targets   : '#clock-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : '+=1000',
        })
        ;
      }
      else{
        this.refs.video.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "clock-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/clock.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/clock.mp4')}
        ></video>
        <div className="row content">
          <div className="col-md-6 offset-md-6 d-flex justify-content-start align-items-center">
            <div className="description scene-value">
              <h4 className="desc">
                In Malaysia, a woman is raped every
              </h4>
              <div>
                <h2 className="timer">00:<span id="clock-counter">00</span>:00</h2>
              </div>

              <div className="bottom-info">
                <div className="legend">
                  <div className="legend-card">
                    <img src={require('./../assets/images/legend/clock-pink.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Raped women</h6>
                  </div>

                  <div className="legend-card">
                    <img src={require('./../assets/images/legend/clock-red.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Victims under 16 years old</h6>
                  </div>
                </div>

                <div className="cta-buttons button-list vertical-buttons">
                  <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                    <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
                  </a>
                  <a href="#" className="btn btn-default btn-sm">
                    <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;