import React, { Component } from 'react';
import './../../styles.scss';
import './styles.scss';
import anime from 'animejs'
import './../../../../../../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done: false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#bands-scene .desc').each(function(){
          $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.refs.video.play();
        }, 500);

        setTimeout(() => {
          $('#cabinet-value').animateNumber({
            number    : 80,
            easing    : 'easeInOutExpo',
          }, 2000);
        }, 3000);

        let bandsAnime = anime.timeline({})
        .add({
          targets   : '#bands-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=5000',
          delay     : function(el, i) {
            return 500 + 30 * i;
          }
        })
        .add({
          targets   : '#bands-scene .legend',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
        })
        .add({
          targets   : '#bands-scene .cta-buttons',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1200,
          offset    : '+=1000',
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1500,
          offset  : '+=1000',
        })
        ;
      }
      else{
        this.refs.video.load();
        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "bands-scene"
        className = {`room-details ${this.props.focused ? 'focused' : ''}`}
        // style     = {{ backgroundImage: `url(${require('./../assets/images/bands.png')})` }}
      >
        <div className="bg-overlay"></div>
        <video
          ref = "video"
          src = {require('./../assets/images/stop-motion/friendshipbands.mp4')}
        ></video>
        <div className="content">
          <div className="scene-value">
            <div className="left-info">
              <h2 className="percentage"><span id="cabinet-value">0</span>%</h2>
              <h4 className="desc">of rape cases begin as an online friendship.</h4>
              <div className="legend vertical-legend">
                <div className="legend-card">
                  <img src={require('./../assets/images/legend/bands-red.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">Internet Acquaintances</h6>
                </div>

                <div className="legend-card">
                  <img src={require('./../assets/images/legend/bands-white.png')} alt="" className="legend-image"/>

                  <h6 className="legend-title">Others</h6>
                </div>
              </div>
            </div>
            <div className="cta-buttons button-list">
              <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                <img src={require('./../assets/images/learn-how-to-stop-button.png')} alt="Digi Predator Stop button"/>
              </a>
              <a href="#" className="btn btn-default btn-sm">
                <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;