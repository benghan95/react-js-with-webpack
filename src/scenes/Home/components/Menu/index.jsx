import React, { Component } from 'react'
import anime from 'animejs'
import { styles } from './styles.scss'

let circle = null;

class Menu extends Component {
	constructor (props){
		super(props);
		this.state = {
			focused       : false,
			didMount      : false,
			circle        : null,
			preloaderEnded: false,
			onButton      : false,
			mainStyle     : {},
			cursorSize    : '35px',
			zoomDark      : false,
		}
	}

	componentDidMount(){
	}

	render() {
		return (
			<section id="menu" className={`navigation row align-items-center justify-content-center ${(this.props.menuOpen) ? '' : 'd-none' }`}>
        <div className="col d-flex flex-row align-items-center justify-content-center">
					<div className="menu-item active">
							<a href="#">Predators<br/>In Plain Sight</a>
					</div>
					<div className="menu-item">
							<a href="#">Tips</a>
					</div>
					<div className="menu-item">
							<a href="#">About</a>
					</div>
				</div>
      </section>
		);
	}
}

export default Menu;