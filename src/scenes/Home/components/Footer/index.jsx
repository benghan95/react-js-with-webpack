import React, { Component } from 'react'
import { styles } from './styles.scss'

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
    }
  }

  _toggleMenuBtn = () => {
    this.setState((prevState) => { return {
      menuOpen: !prevState.menuOpen
    }});
  }

  render() {
    return (
      <section id="footer" className={this.props.className}>
        <div className="footer-right">
          <div className="footer-actions">
            <a id="headphone-icon" className="footer-action" href="#">
              <img src={require('./../assets/images/footer-headphone.png')} alt="Digi Predator Headphone Icon"/>
            </a>
            <a id="share-icon" className="footer-action" href="#">
              <img src={require('./../assets/images/footer-share.png')} alt="Digi Predator Share Icon"/>
            </a>
            <button id="watch-icon" className="footer-action btn btn-link" data-toggle="modal" data-target="#myModal">
              <img src={require('./../assets/images/footer-watch.png')} alt="Digi Predator Watch Icon"/>
            </button>
          </div>



          <div className="footer-owner">
            <div className="project-by">
              <p>A project by </p>
              <a id="" className="digi-logo" href="https://digi.my" target="_blank">
                <img src={require('./../assets/images/digi-logo.png')} alt="Digi Logo"/>
              </a>
              <a id="" className="yellow-heart" href="https://digi.my/yellowheart" target="_blank">
                <img src={require('./../assets/images/digi-yellow-heart.png')} alt="Digi Yellow Heart Logo"/>
              </a>
            </div>
          </div>
        </div>
          <div className="modal modal-video fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header justify-content-end">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                  <div className="embed-responsive embed-responsive-16by9">
                    <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/gTahJpMJK7w"></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </section>

    );
  }
}

export default Footer;