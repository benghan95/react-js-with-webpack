import React, { Component } from 'react';
import anime from 'animejs'
import { styles } from './styles.scss'

class HoverButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      circle  : null,
      position: {},
      onHover : false,
    }
  }

  componentDidMount() {
    // this.setState({
    //   circle: this.props.circle
    // })
  }

  _onMouseMove = (e) => {
    // e.persist();
    // let diffX = e.pageX - e.target.offsetLeft;
    // let diffY = e.pageY - e.target.offsetTop;
    // console.log(e.target.offsetLeft);
    // console.log(e.target);
    // console.log(e);
    // console.log(e.pageX + " - " + e.target.offsetLeft + " = " + diffX);
    // console.log(e.pageY + " - " + e.target.offsetTop + " = " + diffY);
    // this.setState({
    //   position: {
    //     left: diffX + 'px',
    //     top : diffY + 'px',
    //   }
    // })
  }

  _onClick = (e) => {
    $('#dark-room').css("transform-origin", e.clientX + 'px ' + e.clientY + 'px');
    this.props.onHoverDetails(this.props.scene, true);
    this.props.onHoverComplete(true);
  }

  _onMouseEnter = (e) => {
    this.props.onHoverIconCallback(60);
    this.setState({
      onHover: true,
    });

    // this.props.circle.animate(1, { 
    //   duration: 2000,
    // }, () => {
    //   this.props.onHoverComplete(true);
    // });
  }

  _onMouseLeave = () => {
    this.props.onHoverIconCallback(35);
    // this.props.onHoverDetails(this.props.scene, false);
    this.setState({
      onHover: false,
    })
    // this.props.circle.animate(0, { duration: 500 });
    // this.setState({
    //   onHover: false,
    // })
  }


  render() {
    return (
      <div
        id           = {this.props.id}
        className    = {(this.state.onHover) ? 'hover-btn on-hover' : 'hover-btn'}
        onClick      = {(e) => this._onClick(e)}
        onMouseMove  = {(e) => this._onMouseMove(e)}
        onMouseEnter = {(e) => this._onMouseEnter(e)}
        onMouseLeave = {() => this._onMouseLeave()}
      >
        <div className="icon-wrapper">
          <img
            className = "icon"
            style     = {this.state.position}
            src       = {this.props.icon} alt = ""/>
        </div>
      </div>
    );
  }
}

export default HoverButton;