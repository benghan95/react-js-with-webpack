import React, { Component } from 'react'
import anime from 'animejs'
import { styles } from './styles.scss'
import ProgressBar from 'progressbar.js'
import HoverButton from './components/HoverButton'

let moveForce = 20;  // max popup movement in pixels
let itemForce = 2;   // max popup movement in pixels

let circleAnimate = (circle) => {
  circle.animate(1, {
      duration: 1000
  }, function() {
    circle.animate(0, {
          duration: 1000
      }, function() {
        circleAnimate(circle);
      });
  });
}

let circle;

class DarkRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width    : 0,
      height   : 0,
      roomClass: '',
      // roomStyle     : {},
      // roomItemsStyle: {},
      // quoteStyle    : {},
      circle: null,
    }
    // this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.setState({
      circle: this.props.circle,
    })
    // circleAnimate(circle);

    let hoverBtnAnime = anime.timeline({})
    .add({
      targets : '#dark-room .hover-btn',
      opacity : [0, 1],
      easing  : "easeInExpo",
      duration: 1500,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.reached != this.props.reached){
      let hoverBtnAnime = anime.timeline({})
      .add({
        targets : '#dark-room .hover-btn',
        opacity : [0, 1],
        // scale   : [0, 1],
        easing  : "easeOutBack",
        duration: 3000,
        duration: function(el, i, l) {
          return 500 + (i * 500);
        }
      });
    }
  }

  // componentWillUnmount() {
  //   window.removeEventListener('resize', this.updateWindowDimensions);
  // }


  // _onMouseMove = (e) => {
    
  // }

  _onMouseEnter = () => {
    if (this.props.circle){
      this.props.circle.animate(1, { duration: 1500 });
    }
  }
  
  _onMouseLeave = () => {
    if (this.props.circle){
      this.props.circle.animate(0, { duration: 500 });
    }
  }

  onHoverComplete = (state) => {
    if (state){
      // let zoomDetailsTl = new TimelineMax();
      // zoomDetailsTl
      //   .set($('.room-details'), { scale: '1.1' }, 0)
      //   .set($('#dark-room'), { scale: '1' }, 0.5)
      //   .set($('#dark-room'), { scale: '1.05' }, 0.75)
      //   .set($('#dark-room'), { scale: '1.1' }, 1.0)
      //   .set($('#dark-room'), { scale: '1.15' }, 1.25)
      //   .set($('#dark-room'), { scale: '1.2' }, 1.5)
      //   .set($('#dark-room'), { opacity: 0.5 }, 1.5)
      //   .set($('#dark-room'), { opacity: 0 }, 1.75)
      //   .set($('#menu'), { opacity: 0 }, 1.75)

      let zoomDetailsAnime = anime.timeline({})
      .add({
        targets : '#dark-room .on-hover',
        scale   : [1.3, 1.5],
        easing  : "easeInExpo",
        duration: 1000,
      })
      .add({
        targets : '.room-details',
        scale   : '1.1',
        easing  : "easeInExpo",
        duration: 0,
      })
      .add({
        targets : '#dark-room',
        scale   : '1.3',
        easing  : "easeInExpo",
        duration: 1500,
      })
      .add({
        targets : '#dark-room',
        opacity : [1, 0],
        easing  : "easeInExpo",
        duration: 500,
        offset  : '-=500'
      })
      .add({
        targets : '#menu',
        opacity : [1, 0],
        easing  : "easeInExpo",
        duration: 500,
        offset  : '-=500'
      })

      // .add({
      //     targets : '.room-details',
      //     scale   : ['0.8', '1'],
      //     easing  : "easeInExpo",
      //     duration: 500,
      //     offset  : '-=1000'
      // })
      // .add({
      //   targets : '.room-details',
      //   opacity : [0, 1],
      //   easing  : "easeInExpo",
      //   duration: 1000,
      //   offset  : '-=500'
      // })
      // .add({
      //     targets : '.room-details',
      //     scale   : ['0.8', '1'],
      //     easing  : "easeInExpo",
      //     duration: 1000,
      //     offset  : '-=1000'
      // })
      ;

      // zoomDetailsTl.eventCallback("onComplete", () => {
      //   this.props.onZoom(true);
      //   let returnRoom = new TimelineMax();
      //   returnRoom
      //     .set($('#return-room'), { opacity: 0.5 }, 0.25)
      //     .set($('#return-room'), { opacity: 1 }, 0.5)
      //     .set($('#dark-room'), { opacity: 1, scale: 1, }, 0.5)
      // });


      zoomDetailsAnime.complete = () => {
        this.props.onZoom(true);
        anime.timeline({})
        .add({
          targets : '#return-room',
          opacity : [0, 1],
          // easing  : "easeIn",
          duration: 500,
        })
        .add({
          targets : '#dark-room',
          scale   : '1',
          opacity : 1,
          // easing  : "easeIn",
          duration: 0,
        })
        .add({
          targets : '#dark-room .on-hover',
          scale   : 1,
          easing  : "easeInExpo",
          duration: 0,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "dark-room"
        style     = {this.props.roomStyle}
        className = {!this.props.focused ? null : 'focused'}
        // onTouchMove = {e => this._onTouchMove(e)}
        // onMouseMove = {e => this._onMouseMove(e)}
        >
        <div id="room-fixed">
          {/* <div className="instruction">
            <div className="hold-icon">
              <div className="icon-core"></div>
            </div>
            <p><strong>Hover an icon</strong> to begin</p>
          </div> */}
          <div className="quote" style= {this.props.quoteStyle}>
            <h2>Every day, sexual crimes against<br />children occur hidden in plain sight.</h2>
            <h3>Select an icon to take a closer look.</h3>
          </div>
        </div>
        <section id="room-items" style={this.props.roomItemsStyle}>
          <HoverButton 
            id                  = "bed-icon"
            icon                = {require('./../assets/images/icons/bed.svg')}
            scene               = 'bed'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "bands-icon"
            icon                = {require('./../assets/images/icons/friendship-bands.svg')}
            scene               = 'bands'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "board-icon"
            icon                = {require('./../assets/images/icons/board.svg')}
            scene               = 'board'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "cabinet-icon"
            icon                = {require('./../assets/images/icons/hanging-clothes.svg')}
            scene               = 'cabinet'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "shelves-icon"
            icon                = {require('./../assets/images/icons/shelves.svg')}
            scene               = 'shelves'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "clock-icon"
            icon                = {require('./../assets/images/icons/clock.svg')}
            scene               = 'clock'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "lego-icon"
            icon                = {require('./../assets/images/icons/lego.svg')}
            scene               = 'lego'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "fan-icon"
            icon                = {require('./../assets/images/icons/fan.svg')}
            scene               = 'fan'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
          <HoverButton 
            id                  = "trophy-icon"
            icon                = {require('./../assets/images/icons/trophy.svg')}
            scene               = 'trophy'
            circle              = {this.props.circle}
            onHoverComplete     = {this.onHoverComplete}
            onHoverDetails      = {this.props.onHoverDetails}
            onHoverIconCallback = {this.props.onHoverIconCallback}/>
        </section>
      </section>
    );
  }
}

export default DarkRoom;