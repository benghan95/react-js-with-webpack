import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done     : false,
      completed: false,
      videoPlaying  : false,
      audioPlaying  : false,
    }
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#bed-scene .desc').each(function(){
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });
        $('#bed-scene .left-info h4').each(function(){
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');

        // setTimeout(() => {
        //   $('#side-counter').animateNumber({
        //     number    : 22134,
        //     numberStep: comma_separator_number_step,
        //     easing    : 'easeInOutExpo',
        //   }, 2000);
        // }, 5000);

        setTimeout(() => {
          $('#bed-counter').animateNumber({
            number    : 13272,
            numberStep: comma_separator_number_step,
            easing    : 'easeInOutExpo',
          }, 2250);

          this.setState({ videoPlaying: true });
          this.setState({ audioPlaying: true });
        }, 1000);

        let bedAnime = anime.timeline({})
        .add({
          targets : '#bed-scene .legend',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 1000,
        })
        .add({
          targets   : '#side-counter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
        })
        .add({
          targets   : '#bed-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '-=1000',
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets : '#marker-line',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500'
        })
        .add({
          targets   : '#bed-counter',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
          offset    : '-=1000',
        })
        .add({
          targets   : '#bed-scene .left-info .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '+=500',
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets : '#bed-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#bed-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        $('#footer .footer-left').css('opacity', '0');
        // $('#bed-counter').html('0');
        $('#bed-counter').animateNumber({
          number    : 0,
          numberStep: comma_separator_number_step,
          easing    : 'easeInOutExpo',
        }, 250);

        this.setState({
          done: false,
        })
      }
    }
  }

  render() {
    return (
      <section
        id        = "bed-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref         = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className   = 'video-player'
          url         = {this.props.video}
          playing     = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div className="col-md-7 col-sm-7 text-center left">
            <div className="left-info">
              <div className="text-center">
                <h2 className="digits"><span id="bed-counter">0</span></h2>
                <h4>of these cases involved rape.</h4>
              </div>
            </div>
          </div>
          <div id="marker-line">
            <svg id="line-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.9 308.13">
              <polyline points="9.4 156.43 9.4 307.75 0.38 307.75" className="dashed-line"/>
              <polyline points="9.4 154.4 9.4 0.38 0.38 0.38" className="dashed-line"/>
              <g>
                <line x1="36.75" y1="155.43" x2="10.2" y2="155.43" className="dashed-line"/>
                <circle className="fill-white" cx="38.97" cy="155.43" r="0.94" />
              </g>
            </svg>
          </div>
          <div className="col-md-5 col-sm-5 right d-flex justify-content-center align-items-center">
            <div className="right-info">
              <div className="stats">
                <h3><span id="side-counter" className="digits">22,134</span></h3>
                <h4 className="desc">Malaysian children were sexually abused</h4>
                <h4 className="desc">between 2010 and 2017.</h4>
                <div className="legend">
                  <div className="legend-card">
                    <img src={require('./../assets/images/legend/bed-white.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Total Sexual Abuse Cases</h6>
                  </div>

                  <div className="legend-card">
                    <img src={require('./../assets/images/legend/bed-red.png')} alt="" className="legend-image"/>

                    <h6 className="legend-title">Rape Cases</h6>
                  </div>
                </div>
                <div className="cta-buttons button-list vertical-buttons">
                  <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                    <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
                  </a>
                  <a href="#" className="btn btn-default btn-sm btn-share">
                    <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                    <HoverShare scene='bed'/>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;