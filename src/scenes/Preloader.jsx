import React, { Component } from 'react';
import anime from 'animejs';
import ProgressBar from 'progressbar.js';
import Quotes from './../components/Quotes';
import SwipeIcon from './../components/SwipeIcon';
import { isSafari } from "react-device-detect";

let dragDurationNeeded = 50;

let progressBar;

let quoteAnime;

class Preloader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mounted       : false,
      loaded        : false,
      progress      : 0,
      onHold        : false,
      startX        : 0,
      startY        : 0,
      swiped        : 0,
      canRelease    : false,
      quoteCount    : 1,
      swipeClass    : 'swipe fade',
      blurrish      : '0',
      preloaderEnded: false,
    };
  }

  /**
  |--------------------------------------------------
  | Component function
  |--------------------------------------------------
  */
  componentWillReceiveProps(nextProps) {
    if (nextProps.mounted !== this.props.mounted) {
      this.setState({
        mounted: true,
      })
      if (nextProps.mounted && !this.state.mounted){
        $('#statistics h3').each(function(){
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"))
          // $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        progressBar = new ProgressBar.Line('#progress', {
          duration   : 500,
          strokeWidth: 3,
          easing     : 'easeInOut',
          color      : '#fc0e29',
          trailColor : 'transparent',
          trailWidth : 0,
          svgStyle   : {
            display: 'block',
            width  : '100%'
          },
          from: { color: '#fff000' },
          to  : { color: '#ed1b24' },
          step: function(state, circle, attachment) {
              circle.path.setAttribute('stroke', state.color);
          },
        });

        setTimeout(() => {
          this.initQuoteAnime();
          this.loopQuoteAnime();
        }, 1000);

        // setTimeout(() => { this.progressDone() }, 1000);

        setTimeout(() => { this.progressInc() }, 1000);
        setTimeout(() => { this.progressInc() }, 3000);
        setTimeout(() => { this.progressInc() }, 7000);
        setTimeout(() => { this.progressInc() }, 10000);
        setTimeout(() => { this.progressInc() }, 12500);
        setTimeout(() => { this.progressInc() }, 14000);
        setTimeout(() => { this.progressInc() }, 18000);
        setTimeout(() => { this.progressInc() }, 20000);
        setTimeout(() => { this.progressInc() }, 25000);
        setTimeout(() => { this.progressDone() }, 30000);
      }
    }
  }


  /**
  |--------------------------------------------------
  | ProgressBar functions
  |--------------------------------------------------
  */
  progressStart = () => {
    this.setState({
      progress: 0,
    })
  }

  progressInc = () => {
    let max         = 0.9 * 100
    let min         = this.state.progress * 100
    let newProgress = Math.floor(Math.random() * (max - min) + min) / 100

    this.setState({
      progress: newProgress,
    });

    progressBar.animate(this.state.progress, {
      duration: 800
    });
  }

  progressDone = () => {
    progressBar.animate(1, {
      duration: 800
    }, () => {
      this.setState({
        loaded    : true,
      });

      this.setState({
        canRelease: true,
        blurrish  : 3,
      })

      if (!this.state.preloaderEnded){
        this.props.onPassPreloader(true);

        quoteAnime.pause();
        quoteAnime.complete = null;

        this.setState({
          onHold  : false,
          swiped  : 1,
        });

        let preloaderEndAnime = anime.timeline({})
        .add({
          targets : '#preloader .swipe',
          opacity : [1, 0],
          easing  : "easeInExpo",
          duration: 1000,
        })
        .add({
          targets : '#preloader',
          scale   : '1.3',
          easing  : "easeInQuart",
          duration: 1500,
        })
        .add({
          targets : '#preloader',
          opacity : [1, 0],
          easing  : "easeOutCubic",
          duration: 500,
          offset  : '-=250',
        });

        preloaderEndAnime.complete = () => {
          this.setState({
            preloaderEnded: true,
          });
          this.props.preloaderCallback(true);
        }

        // anime.timeline({}).add({
        //   targets : '#preloader',
        //   opacity : 0,
        //   easing  : "easeInOutQuad",
        //   duration: 0,
        // })
      }
    });
  }

  /**
  |--------------------------------------------------
  | Quote Animation functions
  |--------------------------------------------------
  */
  loopQuoteAnime = () => {
    quoteAnime.complete = ()  => {
      let max     = 3;
      let current = this.state.quoteCount;

      if (current < max){
        this.setState( (prevState) => ({
          quoteCount: prevState.quoteCount + 1
        }));
      }
      else{
        this.setState({
          quoteCount: 1
        });
      }

      $('#statistics h3').each(function(){
        $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        // $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
      });

      this.initQuoteAnime();
      this.loopQuoteAnime();
    }
  }

  initQuoteAnime = () => {
    quoteAnime = anime.timeline()
    .add({
      targets   : '#statistics h3 .letter',
      translateX: [40,0],
      translateZ: 0,
      opacity   : [0,1],
      easing    : "easeOutExpo",
      duration  : 3000,
      delay     : function(el, i) {
        return 500 + 30 * i;
      }
    })
    .add({
      targets : '#statistics h5',
      opacity : [0,1],
      easing  : "easeInOutQuad",
      duration: 1500,
      offset  : '-=2000',
    })
    .add({
      targets   : '#statistics h3 .letter',
      translateX: [0,-30],
      opacity   : [1,0],
      easing    : "easeInExpo",
      duration  : 1100,
      offset    : '+=5000',
      delay     : function(el, i) {
        return 100 + 30 * i;
      }
    })
    .add({
      targets : '#statistics h5',
      opacity : 0,
      duration: 1000,
      easing  : "easeOutExpo",
      offset  : '-=1000'
    });
  }

  /**
  |--------------------------------------------------
  | Swipe functions
  |--------------------------------------------------
  */
  onHold = (e) => {
    if (isSafari) {
      if(this.state.loaded){
        if(this.state.canRelease){
          this.setState({
            onHold  : true,
            startX  : e.pageX,
            startY  : e.pageY,
            blurrish: 5,

            swiped: 1,
          });
        }
        else{
          quoteAnime.pause();
          this.setState({
            onHold: true,
            startX: e.pageX,
            startY: e.pageY,

            blurrish: 1,

            swiped: 0,
          });
        }
      }
    }
  }

  onSwipe = (e) => {
    if (isSafari) {
      if (this.state.onHold && this.state.loaded){
        let difference = e.pageX - this.state.startX
        if (difference > 0){
          let blurrish = difference / dragDurationNeeded * 3;
          this.setState({
            swiped: difference / dragDurationNeeded,

            blurrish: 1 + ((blurrish > 5) ? 5   : blurrish),
          });

          if (difference > dragDurationNeeded){
            this.setState({
              canRelease: true,

              blurrish  : 3,
            })
          }
        }
        else{
          this.setState({
            blurrish: 0,
          });
        }
      }
    }
  }

  onRelease = () => {
    if (isSafari) {
      if(this.state.loaded){
        if (this.state.canRelease){
          this.setState({
            onHold: false,
            swiped: 1,

            blurrish: 3,
          });
          if (!this.state.preloaderEnded){
            this.props.onPassPreloader(true);

            let preloaderEndAnime = anime.timeline({})
            .add({
              targets : '#preloader .swipe',
              opacity : [1, 0],
              easing  : "easeInExpo",
              duration: 1000,
            })
            .add({
              targets : '#preloader',
              scale   : '1.3',
              easing  : "easeInQuart",
              duration: 1500,
            })
            .add({
              targets : '#preloader',
              opacity : [1, 0],
              easing  : "easeOutCubic",
              duration: 500,
              offset  : '-=250',
            });

            preloaderEndAnime.complete = () => {
              this.setState({
                preloaderEnded: true,
              });
              this.props.preloaderCallback(true);
            }
          }
        }
        else{
          quoteAnime.play();
          this.setState({
            canRelease: false,
            onHold    : false,
            swiped    : 0,
            blurrish  : 0,
          })
        }
      }
    }
  }

  /**
   |--------------------------------------------------
   | Render function
   |--------------------------------------------------
   */
  render() {
    const preloaderStyle = Object.assign({}, {
      width : `${this.state.width}%`,
      height: `${this.state.height}%`,
    });

		return (
      <section
        id           = "preloader"
        className    = {this.state.preloaderEnded ? 'preloader-ended d-flex' : 'd-flex'}
        // onTouchStart = {e => this.onHold(e.changedTouches[0])}
        // onTouchMove  = {e => this.onSwipe(e.changedTouches[0])}
        // onTouchEnd   = {() => this.onRelease()}
        // onMouseDown  = {e => this.onHold(e)}
        // onMouseMove  = {e => this.onSwipe(e)}
        // onMouseUp    = {() => this.onRelease()}
        // onMouseLeave = {() => this.onRelease()}
      >
        <div className="container-fluid wrapper" style={{filter: 'blur(' + this.state.blurrish + 'px)'}}>
          <div className="row h-100 w-100 m-0" style={{display: (this.state.preloaderEnded) ? 'none' : '' }}>
            <div className="col d-flex flex-column text-center p-0 vertical-center">
              <section id="music-notice" className="d-flex align-items-end justify-content-center">
                <div>
                  <i className="fa fa-headphones icon-headphones"></i>
                  <h6>
                    Best Experienced with Sound
                    {
                      isSafari ? 
                      <span 
                        className={this.props.muted ? 'preloader-unmute' : 'preloader-unmute d-none'}
                        onClick={() => this.props.unmuteSound(true)}
                      >Turn on Sound</span> : null
                    }
                  </h6>
                </div>
              </section>
              <section id="statistics">
                <Quotes quote={this.state.quoteCount}/>
              </section>
            </div>
          </div>
          <div id="progress" className={(this.state.loaded) ? 'fade-out' : ''}></div>
        </div>

        {/* {
          isSafari ? <div className={this.state.swipeClass} >
            <SwipeIcon swiped={this.state.swiped} />
            <p style={{filter: 'blur(' + this.state.blurrish / 5  + 'px)'}}>Swipe to unlock</p>
          </div> : null
        } */}
      </section>
		);
	}
}

export default Preloader;