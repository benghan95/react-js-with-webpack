import React, { Component } from 'react';
import anime from 'animejs';

class Menu extends Component {
	constructor (props){
		super(props);
		this.state = {
			focused       : false,
			didMount      : false,
			preloaderEnded: false,
			onButton      : false,
			cursorSize    : '35px',
			zoomDark      : false,
		}
	}

	componentDidMount(){
	}

	render() {
		return (
			<section id="menu" className={`navigation row align-items-center justify-content-center ${(this.props.menuOpen) ? '' : 'd-none' }`}>
        <div className="col d-flex flex-row align-items-center justify-content-center">
					<div className="menu-item active">
							<a href="#">Hidden<br/>In Plain Sight</a>
					</div>
					<div className="menu-item">
							<a href="preventive-tips/" target="_blank">Preventive<br/>Tips</a>
					</div>
					<div className="menu-item">
							<a href="about" target="_blank">About</a>
					</div>
				</div>
      </section>
		);
	}
}

export default Menu;