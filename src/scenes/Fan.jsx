import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import { TweenMax, TextPlugin, TweenLite, TimelineMax } from "gsap";
import HoverShare from './../components/HoverShare';
import ReactPlayer from 'react-player';

let percentage = 100;

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done           : false,
      completed      : false,
      endVideoPlaying: false,
      videoPlaying   : false,
      audioPlaying   : false,
    }
  }

  changeValue = (newtext) => {
    $("#fan-value").text(newtext);
  }

  componentDidMount = () => {
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused){
        $('#fan-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        setTimeout(() => {
          this.setState({ endVideoPlaying: true });
          this.setState({ videoPlaying: true });
          this.setState({ audioPlaying: true });
        }, 500);

        setTimeout(() => {
          $('#fan-value').prop('number', 0).animateNumber({
            number: 100,
            easing: 'easeInQuad',
          }, 650);
        }, 2000);

        setTimeout(() => {
          $('#fan-value').prop('number', 100).animateNumber({
            number: 200,
            easing: 'easeInQuad',
          }, 650);
        }, 3150);

        setTimeout(() => {
          $('#fan-value').prop('number', 200).animateNumber({
            number: 300,
            easing: 'easeInQuad',
          }, 650);
        }, 4300);

        let fanValue = anime.timeline({})
        .add({
          targets   : '#fan-scene .scene-value',
          translateX: ['-87%', '-70%'],
          scale     : [0.5, 0.65],
          easing    : "linear",
          duration  : 650,
          offset    : '+=2000'
        })
        .add({
          targets   : '#fan-scene .scene-value',
          translateX: '-33%',
          scale     : 0.8,
          easing    : "linear",
          duration  : 650,
          offset    : '+=500'
        })
        .add({
          targets   : '#fan-scene .scene-value',
          translateX: '0%',
          scale     : 1,
          easing    : "linear",
          duration  : 650,
          offset    : '+=500'
        })

        let fanAnime = anime.timeline({})
        .add({
          targets : '#fan-scene .line-1',
          opacity : [0, 1],
          easing  : "linear",
          duration: 150,
          offset  : '+=2000'
        })
        .add({
          targets: '#fan-scene .line-2',
          opacity : [0, 1],
          easing  : "linear",
          duration: 150,
        })
        .add({
          targets: '#fan-scene .line-3',
          opacity : [0, 1],
          easing  : "linear",
          duration: 150,
        })
        .add({
          targets: '#fan-scene .line-4',
          opacity : [0, 1],
          easing  : "linear",
          duration: 150,
        })
        .add({
          targets: '#dotted-line .point-1',
          opacity : [0, 1],
          easing  : "linear",
          duration: 10,
        })
        .add({
          targets : '#fan-scene .line-5',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
          offset  : '+=500'
        })
        .add({
          targets: '#fan-scene .line-6',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
        })
        .add({
          targets: '#fan-scene .line-7',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
        })
        .add({
          targets: '#fan-scene .line-8',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
        })
        .add({
          targets: '#dotted-line .point-2',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 10,
        })
        .add({
          targets : '#fan-scene .line-9',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
          offset  : '+=500'
        })
        .add({
          targets: '#fan-scene .line-10',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
        })
        .add({
          targets: '#fan-scene .line-11',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
        })
        .add({
          targets: '#fan-scene .line-12',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 150,
        })
        .add({
          targets: '#dotted-line .point-3',
          opacity : [0, 1],
          easing  : "easeInQuad",
          duration: 10,
        })
        .add({
          targets   : '#fan-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          offset    : '+=500',
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets : '#fan-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#fan-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ endVideoPlaying: false });
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.endVideoPlayer.seekTo(0);
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        $('#fan-value').html('0');
        this.setState({
          done: false,
          completed: false,
        })
      }
    }
  }

  _videoOnEnded = () => {
    this.setState({
      completed: true,
    })
  }

  render() {
    return (
      <section
        id        = "fan-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.endVideoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.endVideo}
          playing   = {this.state.endVideoPlaying}
          loop      = {true}
        />
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = {this.state.completed ? 'video-player d-none' : 'video-player'}
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
          onEnded   = {() => this._videoOnEnded()}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <svg id="dotted-line" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.08 5.61">
          <g className="line line-1">
            <path d="M13.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M10.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M8.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M5.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M3.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,1,1,0,.75Z" className="fill-white" />
            <path d="M.88,5.56H.38a.38.38,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-2">
            <path d="M28.37,5.61h-.5a.37.37,0,1,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M25.87,5.59h-.5a.37.37,0,1,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M23.37,5.57h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M20.87,5.55h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M18.37,5.54h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,1,1,0,.75Z" className="fill-white" />
            <path d="M15.87,5.52h-.5a.37.37,0,0,1,0-.75h.5a.37.37,0,1,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-3">
            <path d="M43.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M40.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M38.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M35.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M33.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M30.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-4">
            <path d="M58.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M55.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M53.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M50.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M48.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M45.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-5">
            <path d="M73.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M70.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M68.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M65.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M63.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M60.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-6">
            <path d="M88.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M85.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M83.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M80.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M78.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M75.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-7">
            <path d="M103.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M100.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M98.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M95.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M93.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M90.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-8">
            <path d="M115.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M113.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M110.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M108.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M105.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-9">
            <path d="M130.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M128.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M125.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M123.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M120.88,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
            <path d="M118.38,5.56h-.5a.38.38,0,0,1,0-.75h.5a.37.37,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-10">
            <path d="M145.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M143.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M140.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M138.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M135.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M133.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-11">
            <path d="M160.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M158.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M155.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M153.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M150.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M148.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
          </g>
          <g className="line line-12">
            <path d="M173.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M170.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M168.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M165.88,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
            <path d="M163.38,5.56h-.5a.37.37,0,0,1,0-.75h.5a.38.38,0,0,1,0,.75Z" className="fill-white" />
          </g>

          <path className="point point-1 fill-white" d="M58.11,5.09a.38.38,0,0,1-.37-.37v-.5a.38.38,0,0,1,.75,0v.5A.38.38,0,0,1,58.11,5.09Zm0-2.5a.38.38,0,0,1-.37-.37v-.5a.38.38,0,0,1,.75,0v.5A.38.38,0,0,1,58.11,2.59Z" />
          <path className="point point-2 fill-white" d="M115.61,5.09a.38.38,0,0,1-.37-.37v-.5a.38.38,0,1,1,.75,0v.5A.38.38,0,0,1,115.61,5.09Zm0-2.5a.38.38,0,0,1-.37-.37v-.5a.38.38,0,1,1,.75,0v.5A.38.38,0,0,1,115.61,2.59Z" />
          <g className="point point-3">
            <circle cx="173.14" cy="0.94" r="0.94" className="fill-white" />
            <path d="M173.14,3.92a.38.38,0,0,1-.37-.37V3a.38.38,0,0,1,.75,0v.5A.38.38,0,0,1,173.14,3.92Zm0-2.5a.38.38,0,0,1-.37-.37V.55a.38.38,0,1,1,.75,0V1A.38.38,0,0,1,173.14,1.42Z" className="fill-white" />
          </g>
        </svg>

        <div className="row content">
          <div className="col-sm-6 offset-sm-6 d-flex align-items-center justify-content-center">
            <div className="description">
              <h4 className="desc">Within 5 years in Malaysia, </h4>
              <h4 className="desc">Internet-initiated rape</h4>
              <h4 className="desc">cases increased</h4>
              <h2 className="scene-value digits"><span id="fan-value">0</span>%</h2>
              <div className="cta-buttons button-list">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm btn-share">
                  <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                  <HoverShare scene='fan'/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;