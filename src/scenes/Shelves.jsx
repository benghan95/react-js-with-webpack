import React, { Component } from 'react';
import anime from 'animejs';
import './../../node_modules/jquery.animate-number/jquery.animateNumber.min.js';
import ReactPlayer from 'react-player';
import HoverShare from './../components/HoverShare';

class Scene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      done        : false,
      videoPlaying: false,
      audioPlaying: false,
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
    }, 2000);
  };

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focused !== this.props.focused){
      if (this.props.focused) {
        $('#shelves-scene .desc').each(function(){
          // $(this).html($(this).text().replace(/([^]|\w)/g, "<span class='letter'>$&</span>"));
          $(this).html($(this).text().replace(/\b[^\s]+/g, "<span class='letter'>$&</span>"));
        });

        this.setState({ videoPlaying: true });
        this.setState({ audioPlaying: true });

        const counter = document.getElementById('shelves-counter');
        const defaultDigitNode = document.createElement('div');
        defaultDigitNode.classList.add('digit');

        for (let i = 0; i < 10; i++) {
          defaultDigitNode.innerHTML += i + '<br>';
        }

        let currentValue = parseInt(counter.getAttribute('data-value')) || 0;
        var digits = [];
        this.generateDigits(digits, counter, defaultDigitNode, 5);
        this.setValue(digits, counter, defaultDigitNode, 0);

        setTimeout(function() {
          this.setValue(digits, counter, defaultDigitNode, Math.floor(12987));
        }.bind(this), 1500);

        let shelvesAnime = anime.timeline({})
        .add({
          targets   : '#shelves-scene .desc .letter',
          translateX: [40,0],
          translateZ: 0,
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 1000,
          delay     : function(el, i) {
            return 100 + 20 * i;
          }
        })
        .add({
          targets   : '#shelves-line',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
          offset    : '+=4000',
        })
        .add({
          targets   : '#shelves-scene .left-info .percentage',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
          offset    : '-=500',
        })
        .add({
          targets   : '#shelves-scene .center-info h4',
          opacity   : [0,1],
          easing    : "easeOutExpo",
          duration  : 500,
          offset    : '-=500',
        })
        .add({
          targets : '#shelves-scene .btn-learn-stop',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '+=500',
        })
        .add({
          targets : '#shelves-scene .btn-share',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#footer .footer-left',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
        })
        .add({
          targets : '#return-room',
          opacity : [0,1],
          easing  : "easeOutExpo",
          duration: 500,
          offset  : '-=500',
        })
        ;
      }
      else{
        this.setState({ videoPlaying: false });
        this.setState({ audioPlaying: false });
        this.videoPlayer.seekTo(0);
        this.audioPlayer.seekTo(0);
        $('#shelves-counter').html('');
        this.setState({
          done: false,
        })
      }
    }
  }

  setValue = (digits, counter, defaultDigitNode, number) => {
    if (number == 0) {
      var s = '00000'.split('').reverse().join('');
    } else {
      var s = number.toString().split('').reverse().join('');
    }
    var l = s.length;

    if (l > digits.length) {
      this.generateDigits(counter, defaultDigitNode, l - digits.length);
    }

    if (number == 0) {
      for (let i = 0; i < digits.length; i++) {
        this.setDigit(digits, i, s[i] || 0);
      }
    } else {
      for (let j = 0; j < digits.length; j++) {
        setTimeout(function() {
          this.setDigit(digits, j, s[j] || 0);
        }.bind(this), (j * 400));
      }
    }
  }

  setDigit = (digits, digitIndex, number) => {
    switch (number) {
      case '1':
        digits[digitIndex].style.marginTop = '-2.25rem';
        break;
      case '2':
        digits[digitIndex].style.marginTop = '-4.1rem';
        break;
      case '9':
        digits[digitIndex].style.marginTop = '-17.25rem';
        break;
      case '8':
        digits[digitIndex].style.marginTop = '-15.35rem';
        break;
      case '7':
        digits[digitIndex].style.marginTop = '-13.5rem';
        break;
      case '0':
        digits[digitIndex].style.marginTop = '-0.3rem';
        break;
    }
    // digits[digitIndex].style.marginTop = `-${parseFloat(number) + parseFloat((number * 25 + 25) / 100)}rem`;
  }

  generateDigits = (digits, counter, defaultDigitNode, amount) => {
    for (var i = 0; i < amount; i++) {
      var d = defaultDigitNode.cloneNode(true);
      counter.appendChild(d);
      digits.unshift(d);
    }
  }

  render() {
    return (
      <section
        id        = "shelves-scene"
        className = {`room-details ${this.props.focused ? 'focused' : 'd-none'} ${this.props.targetted ? 'targetted' : 'd-none'}`}
      >
        <div className="bg-overlay"></div>
        <ReactPlayer
          ref       = {(c) => { this.videoPlayer = c; }}
          playsinline = {true}
          controls    = {false}
          className = 'video-player'
          url       = {this.props.video}
          playing   = {this.state.videoPlaying}
        />
        <div className='player-wrapper'>
          <ReactPlayer
            ref       = {(c) => { this.audioPlayer = c; }}
            playsinline = {true}
            controls    = {false}
            className = 'react-player'
            url       = {this.props.audio}
            muted     = {this.props.muted}
            volume    = {this.props.detailVolume}
            playing   = {this.state.audioPlaying}
          />
        </div>
        <div className="row content">
          <div className="col-md-6 offset-lg-6 d-flex">
            <div className="description-block">
              <div className="description scene-value">
                <h3 id="shelves-counter" className="digits" data-value="10000"></h3>
                <div className="right-info">
                  <h4 className="desc">cases of child sexual abuse</h4>
                  <h4 className="desc">reported between 2012 and 2016.</h4>
                </div>
              </div>
            </div>

            <div className="center-info">
              <div className="left-info">
                <div id="shelves-line">
                  <img src={require('./../assets/images/line-shelves.png')} alt=""/>
                </div>
                <h2 className="percentage"><span id="shelves-value">1.08</span>%</h2>
                <h4>resulted in convictions.</h4>
              </div>

              <div className="cta-buttons button-list vertical-buttons">
                <a href="preventive-tips/" className="btn-learn-stop" target="_blank">
                  <img src={require('./../assets/images/learn-how-to-stop-button.svg')} alt="Digi Predator Stop button"/>
                </a>
                <a href="#" className="btn btn-default btn-sm btn-share">
                  <img src={require('./../assets/images/footer-share.svg')} alt="Digi Predator Share Icon"/>
                  <HoverShare scene='shelves'/>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Scene;