import React, { Component } from 'react';
import Home from './scenes/Home';
import './assets/scss/style.scss';
import MetaTags from 'react-meta-tags';
import { isBrowser, isMobile, isIE, isEdge } from "react-device-detect";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			width        : 0,
			height       : 0,
			title        : null,
			description  : null,
			scene        : null,
			image        : null,
			screenWarning: false,
			isBrowser    : true,
		}
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}

	componentDidMount = () => {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);

		if (isBrowser) {
			this.setState({ isBrowser: true });
		} else {
			this.setState({ isBrowser: false });
		}

		if (location.search) {
			const query = location.search;
			if (query.indexOf('bed') >= 0) {
				this.setState({
					title      : "PILLOWTALK - What's The Damage?",
					description: "22,134 Malaysian children were sexually abused between 2010 and 2017. 13,272 of these cases involved rape.",
					scene      : 'bed',
					image      : '',
				})
			}
			if (query.indexOf('fan') >= 0) {
				this.setState({
					title      : "ILL WIND - Raise The Alarm",
					description: "Within 5 years in Malaysia, Internet-initiated rape cases increased 300%",
					scene      : 'fan',
					image      : '',
				})
			}
			if (query.indexOf('trophy') >= 0) {
				this.setState({
					title      : "CHAMPION - A Trophy Of Shame",
					description: "Malaysia ranks 3rd in ASEAN for possession and distribution of child pornography. There are no specific laws against this.",
					scene      : 'trophy',
					image      : '',
				})
			}
			if (query.indexOf('board') >= 0) {
				this.setState({
					title      : "CHITCHAT - Their Modus Operandi",
					description: "#1 modus operandi for sexual predators is via chat apps. WeChat is popular because it allows them to search for children nearby.",
					scene      : 'board',
					image      : '',
				})
			}
			if (query.indexOf('bands') >= 0) {
				this.setState({
					title      : "WOVEN LIES - Reality Bytes",
					description: "80% of rape cases begin as an online friendship.",
					scene      : 'bands',
					image      : '',
					})
			}
			if (query.indexOf('shelves') >= 0) {
				this.setState({
					title      : "SKELETONS - Hiding In Here",
					description: "12,987 cases of child sexual abuse reported between 2012 and 2016. 1.08% resulted in convictions.",
					scene      : 'shelves',
					image      : '',
				})
			}
			if (query.indexOf('cabinet') >= 0) {
				this.setState({
					title      : "HUSH NOW - No Child's Play",
					description: "84% of rape cases involve victims 10 - 18 years old.",
					scene      : 'cabinet',
					image      : '',
					})
			}
			if (query.indexOf('clock') >= 0) {
				this.setState({
					title      : "TICKTOCK - And On It Goes",
					description: "In Malaysia, a woman is raped every 15 minutes.",
					scene      : 'clock',
					image      : '',
				})
			}
			if (query.indexOf('lego') >= 0) {
				this.setState({
					title      : "BLOCKS - Relationship-Building",
					description: "90% of sexual predators are people children know, love and trust.",
					scene      : 'lego',
					image      : '',
				})
			}
		}
	};

	updateWindowDimensions() {
		this.setState({
			width: window.innerWidth,
			height: window.innerHeight
		});

		if (window.innerWidth / window.innerHeight < 1.57) {
			this.setState({ screenWarning: true });
		} else {
			this.setState({ screenWarning: false });
		}
	}


	render() {
		const pageStyle = Object.assign({}, {
			width : `${this.state.width}px`,
			height: `${this.state.height}px`,
		});

		return (
			<div
				id="page"
				style={pageStyle}
			>
				<MetaTags>
					<title>{this.state.title ? this.state.title : 'Hidden In Plain Sight'}</title>
					<meta name="title" content={this.state.title ? this.state.title : 'Hidden In Plain Sight'} />>
					<meta name="description" content={this.state.description ? this.state.description : 'Hidden In Plain Sight'} />
					<meta property="og:title" content={this.state.title ? this.state.title : 'Hidden In Plain Sight'} />
					<meta property="og:description" content={this.state.description ? this.state.description : 'Hidden In Plain Sight'} />
					<meta property="og:site_name" content="Hidden In Plain Sight - Digi" />
					<meta property="og:url" content={this.state.scene ? `http://digi-cybersafe.tribalddb.com.my/?scene=${this.state.scene}` : 'http://digi-cybersafe.tribalddb.com.my/'} />
					<meta property="og:image" content={this.state.image ? this.state.image : 'http://digi-cybersafe.tribalddb.com.my/images/thumbnail.jpg'} />
					<meta property="og:type" content="website" />
				</MetaTags>
				{
					isIE || isEdge ? <div id="ie-warning">
						<div className="ie-warning text-center"><h3>Best experienced with sound on Google Chrome and Safari.</h3></div>
					</div> : null
				}
				{
					this.state.screenWarning && this.state.isBrowser ? <div id="screen-warning">
						<div className="screen-warning text-center"><h3>View this in landscape for the full experience.</h3></div>
					</div> : null
				}
				{
					this.state.screenWarning && !this.state.isBrowser ? <div id="screen-warning">
						<div className="screen-warning text-center"><h3>Rotate your device to view in landscape.</h3></div>
					</div> : null
				}
				<Home
					screenWarning = {this.state.screenWarning}
				/>
			</div>
		);
	}
}

export default App;