import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'raw-loader!./index.pug';

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);